<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameChampionLeagueController extends Controller
{
    public function index()
    {
        return view('pages.games.champion_league');
    }
}
