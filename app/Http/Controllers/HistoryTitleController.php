<?php

namespace App\Http\Controllers;

use App\HistoryTitle;
use App\Page;
use Illuminate\Http\Request;

class HistoryTitleController extends Controller
{
    public function index()
    {
        $generalInfo = Page::where('title', 'Titles')->first();
        $titles = HistoryTitle::all();

        return view('pages.history.titles', ['generalInfo' => $generalInfo, 'titles' => $titles]);
    }

//    public function show($manager)
//    {
//        $mainCoach = TeamCoachStaff::where('position', $manager)->first();
//        return view('pages.team.manager', ['manager' => $mainCoach]);
//    }
}
