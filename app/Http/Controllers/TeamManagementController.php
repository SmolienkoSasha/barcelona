<?php

namespace App\Http\Controllers;

use App\TeamManagement;
use Illuminate\Http\Request;

class TeamManagementController extends Controller
{
    public function index()
    {
        $management = TeamManagement::all();
        return view('pages.team.management', ['management' => $management]);
    }
}
