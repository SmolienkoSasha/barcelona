<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TeamCoachStaff;

class TeamCoachingStaffController extends Controller
{
    public function index()
    {
        $manager = TeamCoachStaff::where('position', 'Manager')->first();
        $coachStaff = TeamCoachStaff::where('id', '>', 1)->get();

        return view('pages.team.coaching_staff', ['manager' => $manager, 'coachStaff' => $coachStaff]);
    }

    public function show($manager)
    {
        $mainCoach = TeamCoachStaff::where('position', $manager)->first();
        return view('pages.team.manager', ['manager' => $mainCoach]);
    }
}
