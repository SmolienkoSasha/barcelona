<?php

namespace App\Http\Controllers;

use App\TeamPlayer;
use Illuminate\Http\Request;

class TeamPlayerController extends Controller
{
    public function index()
    {
        $players = $legends = TeamPlayer::where('legend', 0)->get();;
        return view('pages.team.players', ['players' => $players]);
    }

    public function show($id)
    {
        $player = TeamPlayer::where('id', $id)->first();
        return view('pages.team.player', ['player' => $player]);
    }
}
