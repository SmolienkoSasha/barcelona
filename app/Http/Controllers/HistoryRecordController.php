<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HistoryRecordController extends Controller
{
    public function index()
    {
        return view('pages.history.records');
    }
}
