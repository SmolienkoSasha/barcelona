<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameSpainCupController extends Controller
{
    public function index()
    {
        return view('pages.games.spain_cup');
    }
}
