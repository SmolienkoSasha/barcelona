<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class TeamStadiumController extends Controller
{
    public function index()
    {
        $stadiumInfo = Page::where('slug', 'stadium')->first();
        return view('pages.team.stadium', ['stadiumInfo' => $stadiumInfo]);
    }
}
