<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TeamPlayer;

class HistoryLegendController extends Controller
{
    public function index()
    {
        $legends = TeamPlayer::where('legend', 1)->get();
        return view('pages.history.legends', ['legends' => $legends]);
    }

    public function show($id)
    {
        $legend = TeamPlayer::where('id', $id)->first();
        return view('pages.history.legend', ['legend' => $legend]);
    }
}
