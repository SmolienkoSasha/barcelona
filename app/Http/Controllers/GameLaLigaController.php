<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GameLaLigaController extends Controller
{
    public function index()
    {
        return view('pages.games.la_liga');
    }
}
