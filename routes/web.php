<?php

Route::get('/', 'HomeController@index');
Route::get('/news', 'NewsController@index');
Route::get('/gallery', 'GalleryController@index');

Route::get('/history/titles', 'HistoryTitleController@index');
Route::get('/history/records', 'HistoryRecordController@index');
Route::get('/history/legends', 'HistoryLegendController@index');
Route::get('/history/legend/{id}', 'HistoryLegendController@show');

Route::get('/team/players', 'TeamPlayerController@index');
Route::get('/team/player/{id}', 'TeamPlayerController@show');
Route::get('/team/coaching_staff', 'TeamCoachingStaffController@index');
Route::get('/team/coaching_staff/{manager}', 'TeamCoachingStaffController@show');
Route::get('/team/management', 'TeamManagementController@index');
Route::get('/team/stadium', 'TeamStadiumController@index');

Route::get('/games/la_liga', 'GameLaLigaController@index');
Route::get('/games/champion_league', 'GameChampionLeagueController@index');
Route::get('/games/spain_cup', 'GameSpainCupController@index');
