@extends('index')

@section('content')
    <script>
        document.title = 'Gallery';
    </script>

    <h1>Gallery</h1>
    @foreach($gallery as $picture)
        <img src="{{ $picture->src }}" alt="{{ $picture->alt }}" /><br/>
    @endforeach

@endsection
