@extends('index')

@section('title')
    {{ $legend->name }}
@endsection

@section('css')
    {{ asset('css/player.css') }}
@endsection

@section('content')
    <img class="playerPhoto" src="{{ $legend->img_src }}" alt="{{ $legend->img_alt }}" /><br/>
    <h1>{{ $legend->name }}</h1>
    <div>{!! $legend->content !!}</div>
    <br/>
@endsection
