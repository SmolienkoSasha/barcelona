@extends('index')

@section('title')
    {{ $generalInfo->title }}
@endsection

@section('css')
{{--    {{ asset('css/player.css') }}--}}
@endsection

@section('content')
    <h1>{{ $generalInfo->name }}</h1>
    <div>
        <img src="{{ $generalInfo->img_src }}" alt="{{ $generalInfo->img_alt }}" />
        <p>{{ $generalInfo->content }}</p>
    </div><br/><br/><br/>

    @foreach($titles as $title)
        <div>
            <img src="{{ $title->img_src }}" alt="{{ $title->img_alt }}" />
            <h3>{{ $title->name }}</h3>
            <p>{{ $title->content }}</p>
        </div>
    @endforeach
@endsection
