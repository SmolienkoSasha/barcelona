@extends('index')

@section('title')
    Legends
@endsection

@section('css')
    {{ asset('css/player.css') }}
@endsection

@section('content')
    <h1>Legends</h1>
    @foreach($legends as $legend)
        <a class='playerMain' href="/history/legend/{{ $legend->id }}">
            @include('pages.playerLegend', ['item' => $legend])
        </a>
    @endforeach
@endsection
