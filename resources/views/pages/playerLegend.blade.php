{{--<a class='playerMain' href="/team/player/{{ $item->id }}">--}}
    <div class="player" style="background-image: url({{ $item->img_src }});">
        <h2>{{ $item->name }}</h2>
        {{--                    <img class="playerPhoto" src="{{ $player->img_src }}" alt="{{ $player->img_alt }}" /><br/>--}}
        <ul>

            <li>Country: {{ $item->country }}</li>
            <li>Position: {{ $item->position }}</li>
            @if($item->number)
                <li>Date of birth: {{ $item->date_of_birth }}</li>
            @endif
            @if($item->number)
                <li>Number: {{ $item->number }}</li>
            @endif
            <li>Work foot: {{ $item->work_foot }}</li>
            @if($item->number)
                <li>Weight: {{ $item->weight }}</li>
            @endif
            @if($item->number)
                <li>Height: {{ $item->height }}</li>
            @endif
            @if($item->club_debut !== null)
                <li>Club debut: {{ $item->club_debut }}</li>
            @endif
        </ul>
    </div>
{{--</a>--}}
<br/>
