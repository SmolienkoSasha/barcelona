@extends('index')

@section('content')
@section('content')
    <script>
        document.title = 'News';
    </script>

    <h1>News</h1>
    @foreach($news as $article)
        <h2>{{ $article->title }}</h2>
        <img src="{{ $article->img_src }}" alt="{{ $article->img_alt }}" /><br/>
        {!! $article->content !!}
        <br/>
    @endforeach

@endsection
@endsection
