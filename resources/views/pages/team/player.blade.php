@extends('index')

@section('title')
    {{ $player->name }}
@endsection

@section('css')
    {{ asset('css/player.css') }}
@endsection

@section('content')
    <h1>{{ $player->name }}</h1>
    <div>{!! $player->content !!}</div>
    <br/>
@endsection
