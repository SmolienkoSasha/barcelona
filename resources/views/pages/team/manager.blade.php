@extends('index')

@section('title')
    {{ $manager->name }}
@endsection

@section('css')
    {{ asset('css/player.css') }}
@endsection

@section('content')
    <h1>{{ $manager->name }}</h1>
    <div>{!! $manager->content !!}</div>
    <br/>
@endsection
