@extends('index')

@section('title')
    Players
@endsection

@section('css')
    {{ asset('css/player.css') }}
@endsection

@section('content')
    <h1>Players</h1>
    @foreach($players as $player)
        <a class='playerMain' href="/team/player/{{ $player->id }}">
            @include('pages.playerLegend', ['item' => $player])
        </a>
{{--        <a class='playerMain' href="/team/player/{{ $player->id }}">--}}
{{--            <div class="player" style="background-image: url({{ $player->img_src }};">--}}
{{--                <h2>{{ $player->name }}</h2>--}}
{{--                    <img class="playerPhoto" src="{{ $player->img_src }}" alt="{{ $player->img_alt }}" /><br/>--}}
{{--                <ul>--}}
{{--                    <li>Date of birth: {{ $player->date_of_birth }}</li>--}}
{{--                    <li>Country: {{ $player->country }}</li>--}}
{{--                    <li>Position: {{ $player->position }}</li>--}}
{{--                    <li>Number: {{ $player->number }}</li>--}}
{{--                    <li>Work foot: {{ $player->work_foot }}</li>--}}
{{--                    <li>Weight: {{ $player->weight }}</li>--}}
{{--                    <li>Height: {{ $player->height }}</li>--}}
{{--                    @if($player->club_debut !== null)--}}
{{--                        <li>Club debut: {{ $player->club_debut }}</li>--}}
{{--                    @endif--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </a>--}}
{{--        <br/>--}}
    @endforeach
@endsection
