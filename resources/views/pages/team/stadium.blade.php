@extends('index')

@section('title')
    {{ $stadiumInfo->title }}
@endsection

@section('css')
{{--    {{ asset('css/player.css') }}--}}
@endsection

@section('content')
    <h1>{{ $stadiumInfo->name }}</h1>
    <img src="{{ $stadiumInfo->img_src }}" alt="{{ $stadiumInfo->img_alt }}" /><br/>
    <div>{!! $stadiumInfo->content !!}</div>
@endsection
