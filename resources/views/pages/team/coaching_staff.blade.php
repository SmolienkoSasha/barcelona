@extends('index')

@section('title')
    Coaching Staff
@endsection

@section('css')
{{--    {{ asset('css/player.css') }}--}}
@endsection

@section('content')
    <h1>Coaching Staff</h1>
    <a class='playerMain' href="/team/coaching_staff/{{ $manager->position }}">
        <div class="player">
            <img class="playerPhoto" src="{{ $manager->img_src }}" alt="{{ $manager->img_alt }}" /><br/>
            <h2>{{ $manager->name }}</h2>
        </div>
    </a>
    <br/>

        @foreach( $coachStaff as $itemCoachStaff)
            <p>{{ $itemCoachStaff->position }} - {{ $itemCoachStaff->name }}</p>
        @endforeach
@endsection
