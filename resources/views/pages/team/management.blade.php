@extends('index')

@section('title')
    Management
@endsection

@section('css')
    {{--    {{ asset('css/player.css') }}--}}
@endsection

@section('content')
    <h1>Structure</h1>
    @foreach($management as $itemManagement)

        <h3>{{ $itemManagement->category }}</h3>
        <div>{!! $itemManagement->content !!}</div>
        <br/><br/><br/>
    @endforeach
@endsection
