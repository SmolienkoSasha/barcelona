<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="@yield('css')">
    <style>
        header nav ul li {
            display: inline;
        }

        header nav ul li ul {
            display: none;

        }

        header nav ul li:hover ul,
        .main-item:focus ~ .sub-menu,
        .main-item:active ~ .sub-menu,
        .sub-menu:hover
        {
            display: block;
            background: yellow;
            width: 10%;
        }
        header nav ul li ul li a{
            display: block;
        }


        /*.sub-menu {*/
        /*    display: none;*/
        /*}*/
        /*.main-item:focus ~ .sub-menu,*/
        /*.main-item:active ~ .sub-menu,*/
        /*.sub-menu:hover {*/
        /*    display: block;*/
        /*}*/
    </style>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a class="navHome" id="home" href="/" >
                        <img src="{{asset('images/i.png')}}" alt="logo" id='logo'/>
                    </a>
                </li>

                <li>
                    <a href="/news">News</a>
                </li>

                <li>
                    <a class="main-item" href="javascript:void(0);">Team</a>
                    <ul class="sub-menu">
                        <li><a href="/team/players">players</a></li>
                        <li><a href="/team/coaching_staff">coaching staff</a></li>
                        <li><a href="/team/management">management</a></li>
                        <li><a href="/team/stadium">stadium</a></li>
                    </ul>
                </li>

                <li>
                    <a class="main-item" href="javascript:void(0);">Games</a>
                    <ul class="sub-menu">
                        <li><a href="/games/la_liga">la liga</a></li>
                        <li><a href="/games/champion_league">champion league</a></li>
                        <li><a href="/games/spain_cup">spain cup</a></li>
                    </ul>
                </li>

                <li>
                    <a class="main-item" href="javascript:void(0);">History</a>
                    <ul class="sub-menu">
                        <li><a href="/history/titles">titles</a></li>
                        <li><a href="/history/records">records</a></li>
                        <li><a href="/history/legends">legends</a></li>
                    </ul>
                </li>

                <li>
                    <a href="/gallery">Gallery</a>
                </li>
        </nav>
    </header>

    <div id='AnimateContent'>
        @yield('content')
    </div>

    <footer>FOOTER</footer>

</body>
</html>
