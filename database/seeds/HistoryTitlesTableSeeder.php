<?php

use Illuminate\Database\Seeder;

class HistoryTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('history_titles')->insert([
            [
                'name' => 'Champions League - 5',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/59ea52bd-1c5f-4988-95f7-b1ecc64916d4/Champions-League.png',
                'img_alt' => 'Champions League',
                'content' => '1991-92, 2005-06, 2008-09, 2010-11, 2014-15',
            ],
            [
                'name' => 'FIFA Club World Cup - 3',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/3a2e8c01-9bc0-4337-9435-a85019fcbc35/Fifa-Club-World-Cup.png',
                'img_alt' => 'FIFA Club World Cup',
                'content' => '2009-10, 2011-12, 2015-16',
            ],
            [
                'name' => 'European Cup Winners\' Cup - 4',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/1dc08c65-6d38-4d63-b0c5-fe73aafccf33/Recopa-d-Europa.jpg',
                'img_alt' => 'European Cup Winner Cup',
                'content' => '1978-79, 1981-82, 1988-89, 1996-97',
            ],
            [
                'name' => 'Fairs Cup - 3',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/c42ca9a8-73d4-4704-82ec-b211a592f5bd/Copa-de-Fires.jpg',
                'img_alt' => 'Fairs Cup',
                'content' => '1957-58, 1959-60, 1965-66 (won outright in 1971)',
            ],
            [
                'name' => 'European Super Cup - 5',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/5a28f51b-1f2a-4ec1-8475-02ae650a4e13/European-Super-Cup.png',
                'img_alt' => 'European Super Cup',
                'content' => '1992-93, 1997-98, 2009-10, 2011-12, 2015-16',
            ],
            [
                'name' => 'Latin Cup - 2',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/93c22f21-e51d-43c6-80c1-39a2ea5c85a4/Copa-Llatina.jpg',
                'img_alt' => 'Latin Cup',
                'content' => '1948-49, 1951-52',
            ],
            [
                'name' => 'Spanish League Championship - 26',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/119a9766-0db7-4fe1-9f8a-f9fb9dca0ca3/la-liga.png',
                'img_alt' => 'Spanish League Championship',
                'content' => '1928-29, 1944-45, 1947-48, 1948-49, 1951-52, 1952-53, 1958-59, 1959-60, 1973-74, 1984-85, 1990-91, 1991-92, 1992-93, 1993-94, 1997-98, 1998-99, 2004-05, 2005-06, 2008-09, 2009-10, 2010-11, 2012-13, 2014-15, 2015-16, 2017-18, 2018-19',
            ],
            [
                'name' => 'Copa del Rey - 30',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/07e7774e-cf87-49b4-b2ce-f56d62967bce/Spanish-Cup.png',
                'img_alt' => 'Copa del Rey',
                'content' => '1909-10, 1911-12, 1912-13, 1919-20, 1921-22, 1924-25, 1925-26, 1927-28, 1941-42, 1950-51, 1951-52, 1952-53, 1956-57, 1958-59, 1962-63, 1967-68, 1970-71, 1977-78, 1980-81, 1982-83, 1987-88, 1989-90, 1996-97, 1997-98, 2008-09, 2011-12, 2014-15, 2015-16, 2016-17, 2017-18',
            ],
            [
                'name' => 'Spanish Super Cup - 13',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/7db0228d-5e88-465b-82db-61654d4bbd4d/Supercopa-Espanya.jpg',
                'img_alt' => 'Spanish Super Cup',
                'content' => '1983-84, 1991-92, 1992-93, 1994-95, 1996-97, 2005-06, 2006-07, 2009-10, 2010-11, 2011-12, 2013-14, 2016-17, 2018-19',
            ],
            [
                'name' => 'Spanish League Cup - 2',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/617cf9b9-1477-4bcf-af1e-b2428238240a/Copa-de-la-Lliga.jpg',
                'img_alt' => 'Spanish League Cup',
                'content' => '1982-83, 1985-86',
            ],
            [
                'name' => 'Catalan League Championship - 23',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/dd45f7f5-4571-4311-9753-330e93da1777/Campionat-de-Catalunya.jpg',
                'img_alt' => 'Catalan League Championship',
                'content' => '1901-1902, 1902-03, 1904-05, 1908-09, 1909-10, 1910-11, 1912-13, 1915-16, 1918-19, 1919-20, 1920-21, 1921-22, 1923-24, 1924-25, 1925-26, 1926-27, 1927-28, 1929-30, 1930-31, 1931-32, 1934-35, 1935-36, 1937-38 (includes Copa Macaya (1901-02) and Copa Barcelona (1902-03)',
            ],
            [
                'name' => 'Catalan Super Cup - 2',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/2df809e7-c846-4959-8b6d-1d121cca4a89/Supercopa-Catalunya.jpg',
                'img_alt' => 'Catalan Super Cup',
                'content' => '2014-15, 2017-18',
            ],
            [
                'name' => 'Catalan Cup - 8',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/cb38ee48-7b14-443e-97fc-bba554c0be87/Copa-Catalunya.jpg',
                'img_alt' => 'Catalan Cup',
                'content' => '1990-91, 1992-93, 1999-00, 2003-04, 2004-05, 2006-07, 2012-13, 2013-14 (until 1993-94, Copa Generalitat)',
            ],
            [
                'name' => 'Eva Duarte Cup - 3',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/17/918fe117-ae19-4d3e-9f7b-df9b7cf0774a/Eva-Duarte.jpg',
                'img_alt' => 'Eva Duarte Cup',
                'content' => '1948-49, 1951-52, 1952-53',
            ],
            [
                'name' => 'Pyrenees Cup - 4',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/5938ae0d-6861-4dab-acce-105f1fb8c418/Trophy-Silhouette.png',
                'img_alt' => 'Pyrenees Cup',
                'content' => '1909-10, 1910-11, 1911-12, 1912-13',
            ],
            [
                'name' => 'Mediterranean League',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/5938ae0d-6861-4dab-acce-105f1fb8c418/Trophy-Silhouette.png',
                'img_alt' => 'Mediterranean League',
                'content' => '1937',
            ],
            [
                'name' => 'Catalan League - 1',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/08/20/5938ae0d-6861-4dab-acce-105f1fb8c418/Trophy-Silhouette.png',
                'img_alt' => 'Catalan League',
                'content' => '1937-38',
            ],
        ]);
    }
}
