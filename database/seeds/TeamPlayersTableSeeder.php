<?php

use Illuminate\Database\Seeder;

class TeamPlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team_players')->insert([
            [
                'name' => 'Lionel Messi',
                'country' => 'Argentina',
                'date_of_birth' => '24/06/1987',
                'weight' => '72kg',
                'height' => '170cm',
                'club_debut' => '16/10/2004',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/43931b52-6dcb-4f1e-8fba-81e5b688f11a/10_MESSI_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Leo Messi',
                'position' => 'forward',
                'work_foot' => 'left',
                'number' => 10,
                'legend' => 0,
                'content' => '
                <p>Leo Messi\'s footballing career started in 1995 at Newell\'s Old Boys, where he played until the year 2000. At the age of 13, Lionel Messi crossed the Atlantic to try his luck in Barcelona, and joined the Under 14s. Messi made spectacular progress at each of the different age levels, climbing through the ranks to Barça C, followed by Barça B and the first team in record time.</p>

                <p>In the 2003-2004 season, when he was still only 16, Messi made his first team debut in a friendly with Porto that marked the opening of the new Do Dragao stadium.</p>

                <p>The following championship-winning season, Messi made his first appearance in an official match on October 16, 2004, in Barcelona\'s derby win against Espanyol at the Olympic Stadium (0-1). With several first team players seriously injured, the services of several reserve team players were called upon, and Messi became a regular feature of Barça squads.</p>

                <p>On May 1, 2005, Messi became the youngest player ever to score a league goal for FC Barcelona - against Albacete when he was only 17 years, 10 months and 7 days old. That record would eventually be beaten by Bojan Krkic.</p>

                <p>At the Under 20 World Cup in Holland, Messi not only won the title with Argentina, but was also the leading goalscorer and was voted best player in the tournament. Aged 18 years, he had become one of the hottest properties in the world game. Shortly after, he made his first full international appearance in a friendly against Hungary.</p>

                <p>His breakthrough came in the 2005-06 season, starting with an amazing performance in the Joan Gamper Trophy match against Juventus. He was also outstanding at the Santiago Bernabéu, in Barcelona\'s unforgettable 3-0 win, and also at Stamford Bridge, in the Champions League match against Chelsea. Injury kept him sidelined for much of the latter stage of the season, but Messi still played a total of 17 league games, 6 in the Champions League and 2 in the Copa del Rey, and scored eight goals.</p>

                <p>The following season Messi moved up a gear and astounded the world with goals such as the one he scored against Getafe in the Copa del Rey. In the 2006/07 season, and even though the team didn\'t win any titles, the Argentine was second in the FIFA World Player awards and third in the Ballon d’Or. He continued to develop in the 2007/08 campaign, when he scored 16 goals and gave 10 assists in the 40 games he played in. In 2008, Leo Messi was runner up in the FIFA World Player awards for the second season in a row.</p>

                <p>In the 2008/09 season, and now without Ronaldinho alongside him, Messi became the main star of the Barça show. He managed to stay injury free all season, and played 51 games, scoring 38 goals. The Argentinian was also fundamental in the Copa del Rey and Champions League finals, scoring Barça\'s second goals in both. In 2009, he finally won both the FIFA World Player and Ballon d’Or.</p>

                <p>How far can Leo Messi go? He was the league\'s top scorer in the 2009-10 season and equalled Ronaldo\'s historic total of 34 goals (96-97). He scored the goal against Estudiantes that won Barça their first Club World Cup.</p>

                <p>Still hungry for success, the Argentinian went even further in the 2010/11 season, scoring no fewer than 53 official goals, a Spanish record only matched by Cristiano Ronaldo (that very same season). Messi, as in Rome, played a vital role in the Champions League final at Wembley were scored a scorcher from outside the area to put his team ahead. In 2011, he also won the Ballon d’Or for the third time, a feat only previously achieved by Cruyff, Platini and Van Basten and a year later became the first player ever to win it four times.</p>

                <p>The season 2011/12 is when Messi moved past César Rodríguez\'s record of 232 official goals to become the Club\'s all time top goalscorer. He achieved this on the 20 March 2012 in a 5-3 victory over Granada in which he scored a brilliant hat-trick.</p>

                <p>Two weeks earlier, on March 7, 2012, the football world watched in awe as he scored five goals in a single game against Bayer 04 Leverkusen.</p>

                <p>On May 5, 2012, the Leo Messi legend was extended in the derby against Espanyol, when he made it to an unprecedented 50 goals in a league season, scoring four goals in a game for the third time in his career. A remarkable season ended with one of the finest goals of his career in the Copa del Rey final against Athletic Club Bilbao. In the 2011/12 season he has scored in every competition he played in, totally an astonishing 73 as Barça conquered the Spanish Super Cup, European Super Cup, Club World Cup and the Copa del Rey. He ended 2012 with the record number of goals in a calendar year (91), thus beating the historic tally established by Gerd Müller (85 for Bayern Munich and Germany in 1972).</p>

                <p>That was not the only outstanding achievement by Messi in the 2012/13 season, another for the record books. The Argentine scored in 19 consecutive league matches - half a season - until an injury at away at PSG meant he was not at his best in the decisive phase of that campaign’s Champions League. On the domestic front, Messi was also key in Barça claiming the league title with 100 points. He was top scorer with 46 goals in the league, ending with 60 in 50 games in all competitions.</p>

                <p>The following year, 2013/14, was difficult one for the Barça number 10, due to injuries more than anything else. Messi picked up a hamstring injury in November that forced him to miss more than a month of the campaign. Nevertheless, he still scored 41 goals in 46 matches that year, 28 in the league, 5 in the Copa del Rey and 8 in the Champions League.</p>

                <p>Messi made his 250th league appearance in that season and in a game in the Copa del Rey against Levante, he played in his 400th match in a Barça shirt, having recorded a staggering 331 goals at that point in his career for the blaugranes.</p>

                <p>In the same season Messi reached yet another landmark, overtaking Paulino Alcántara’s total of 369 goals to become the leading goalscorer of all time at the Club</p>

                <p>The 2014/2015 season was one of the Argentine star\'s most brilliant. Messi was one of the great architects of the second Treble in club history, six years after winning the League, the Spanish Cup, and the UEFA Champions League in the same season. Thanks to his 56 goals in all three competitions, Messi established two almost unattainable records. First, in a game against Sevilla at Camp Nou Messi surpassed Telmo Zarra\'s 251 career goals to become the all-time leading scorer in league history. Second, with a hat-trick at APOEL Nicosia, Messi passed Raúl\'s 71 career goals to become the all-time leading scorer in Champions League history. Messi closed the season with 77 goals in 99 career Champions League games. The Argentine’s amazing form had its reward on a personal level when in January 2016 he claimed the Ballon d’Or award for an unprecedented fifth time in his career.</p>

                <p>Messi also led the tournament in scoring for the fifth time — tied with Neymar and Cristiano Ronaldo at 10 each. The Argentinian also surpassed Luis Figo\'s all-time La Liga assist record (105).</p>

                <p>On 23 April, on Saint Jordi’s day, Leo Messi reached 500 goals with the first team thanks to his winner in the final minute of the Clásico in the league at the Santiago Bernabéu (2-3).</p>

                <p>Messi is also captain of the Argentina national side and has played in four World Cups (2006, 2010, 2014, 2018), losing in the final in 2014 in Brazil against Germany. He has also played in four Copa Américas (2007, 2011, 2015 and 2016), losing in the final in both 2015 and 2016 to Chile on penalties. In the summer of 2008 he also played at the Beijing Olympics, and came home with a gold medal.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>3 World Club Cups (2009/10, 2011/12 and 2015/16)</li>
                      <li>4 Champions Leagues (2005/06, 2008/09, 2010/11 and 2014/15)</li>
                      <li>10 La Liga (2004/05, 2005/06, 2008/09, 2009/10, 2010/11, 2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>6 Copas del Rey (2008/09, 2011/12, 2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>8 Spanish Super Cups (2005/06, 2006/07, 2009/10 , 2010/11, 2011/12, 2013/14, 2016/17, 2018/19)</li>
                      <li>3 European Super Cup (2009/10, 2011/12, 2015/16)</li>
                      <li>Under-20 World Cup (2004/05)</li>
                      <li>Olympic Games Gold Medal (2008)</li>
                      <li>6 FIFA Ballon d\'Or (2009, 2010, 2011, 2012, 2015, 2019)</li>
                      <li>FIFA World Cup Golden Ball (2014)</li>
                      <li>FIFA World Player (2009)</li>
                      <li>6 Golden Shoe (2009/10, 2011/12, 2012/13, 2016/17, 2017/18, 2018/19)</li>
                      <li>2 Best Player in Europe (2010/11, 2014/15)</li>
                      <li>6 La Liga Top Scorer (2009/10, 2011/12, 2012/13, 2016/17, 2017/18, 2018/19)</li>
                      <li>6 Champions League Top Scorer (2008/09, 2009/10, 2010/11, 2011/12, 2014/15, 2018/19)</li>
                  </ul>
                  '
            ],
            [
                'name' => 'Luis Suárez',
                'country' => 'Uruguay',
                'date_of_birth' => '24/01/1987',
                'weight' => '86kg',
                'height' => '182cm',
                'club_debut' => '25/10/14',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/d3344da5-26c3-4ca2-ae85-70621366edd4/09_SUAREZ_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Luis Suárez',
                'position' => 'forward',
                'work_foot' => 'right',
                'number' => 9,
                'legend' => 0,
                'content' => '
                <p>Luis Alberto Suárez Díaz came to the Camp Nou in the summer of 2014 as holder of the Golden Boot and one of the biggest stars in world football. The striker was born in Salto (Uruguay) on January 24 1987, and has signed for five seasons.</p>

                <p>He started out at Club Nacional de Football in Montevideo, where he played until 2006, before moving to Holland to join Groningen at the tender age of 19. A season later he switched to Ajax Amsterdam, where he was soon one of the biggest stars in Dutch football. He was top scorer in the 2009/10 Eredivisie and led Ajax into the Champions League, in late 2010 he played in his first five games in Europe\'s top club competition. Shortly after, in 2011, he headed for Liverpool.</p>

                <p>His goals soon won him a starting place on Merseyside, where he continued his record of always scoring at least ten goals a season in his professional career, be that in Uruguay, the Netherlands or the UK. At Anfield, his stats improved along with his team’s performances, which almost resulted in Liverpool winning the championship for the first time in 24 years. In a wonderful last season, he scored 31 goals in 33 games to share the Golden Boot award with Cristiano Ronaldo.</p>

                <p>Suárez made his official debut for the Club on 25 Cctober 2014 in La Liga against Real Madrid. The match ended with a Barça defeat (3-1). While it was not the most warming welcome to official competition with his new team, the Uruguay striker would go on to prove his worth throughout the rest of the season.</p>

                <p>Just a month after his debut, Suárez scored his first official goal. It was his sixth game for the club and opened the scoring in a 4–0 Champions League rout at APOEL. He later scored his first goal at Camp Nou on 10 December, in Barça\'s 3–1 win over PSG.</p>

                <p>On 15 February, Barça routed Levante at Camp Nou by a score of 5–0 with the highlight of the day coming as Suárez launched himself skyward for a spectacular scissors-kick goal, finishing an Adriano cross into the back of the net. Nine days later, Suárez scored twice in the Champions League last-16 first leg at Manchester City, and then reappeared again on 15 April in the quarter-final first leg against PSG at Parc des Princes, scoring a spectacular goal after nutmegging David Luiz and sending the ball into the upper right corner from outside the area.</p>

                <p>Back in La Liga, Suárez was actively involved in the scoring in Barça\'s 8–0 victory at Córdoba on 2 May, netting his first hat-trick for the Club. The Uruguayan scored the second, fourth and eighth goals of the match. Two weeks later, on 17 May, despite missing the match against Atlético Madrid with a left hamstring injury, Suárez celebrated his first title with FC Barcelona. Nevertheless, Suárez returned to action on 30 May and duly celebrated Barça\'s 3–1 victory in the Copa del Rey final against Athletic Club Bilbao.</p>

                <p>And to top it all off, Suárez once again showed his decisiveness on the season\'s biggest stage, the UEFA Champions League Final. When Juventus drew level at 1–1 in the second half, Luis Suárez showed his clutch scoring instinct with a perfect finish following a Buffon save on a shot by Messi, putting Barça back up by 2–1 and making the last 20 minutes of the match whole different game.</p>

                <p>After overcoming some early season adversity and proving he could adapt to his new team, Suárez would go on to form the most dangerous goal scoring trio in history. His on- and off-field relationship with Lionel Messi and Neymar was one of the storylines of the season, as the so-called "trident" set records and became the envy of the world, helping propel FC Barcelona to a historic second treble in Club history after winning La Liga, the Copa del Rey and the UEFA Champions League. Suárez played no small role the team\'s unprecedented success.</p>

                <p>Luis Suárez is also the main man in his national team. He led Uruguay to the Copa América 2011 title, where he was also named Player of the Tournament. Along with Diego Forlán and Edinson Cavani, he also played a key role in getting Uruguay into the 2010 World Cup semi-finals. He also took part in the 2014 and 2018 World Cups as well as the 2016 Copa América. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>World Club Cup (2015/16)</li>
                      <li>Champions League (2014/15)</li>
                      <li>4 La Liga (2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>4 Copas del Rey (2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>2 Spanish Super Cups (2016/17, 2018/19)</li>
                      <li>European Super Cup (2015/16)</li>
                      <li>Copa América (2011)</li>
                      <li>Golden Shoe (2013/14)</li>
                      <li>La Liga Top Scorer (2015/16)</li>
                  </ul>
                  '
            ],
            [
                'name' => 'Antoine Griezmann',
                'country' => 'France',
                'date_of_birth' => '21/03/1991',
                'weight' => '73kg',
                'height' => '176cm',
                'club_debut' => '16/08/2019',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/82614aaa-24ce-47ea-a0bb-ffda61abc83c/17_GRIEZMANN_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Antoine Griezmann',
                'position' => 'forward',
                'work_foot' => 'left',
                'number' => 17,
                'legend' => 0,
                'content' => '
                <p>Born on 21 March 1991 in Mâcon, France, Antoine Griezmann is one of the best players in the world. The striker came to Barça in the summer of 2019 after consolidating himself as one of the top footballers in La Liga after spells with Real Sociedad and Atlético Madrid.</p>

                <p>Scouted at an early by the Basque club Real Sociedad, he headed for San Sebastián in 2005 at the age of just 14 after beginning his career with local side Macconial. In Donostia, he made his first team debut, in the Second Division, at the age of just 18 in 2009. Griezmann was one of the key figures in the return to the First Division of Real Sociedad a spell in the Second Division. In the Basque Country he played 202 games for the txuri urdin, scoring 52 goals. The Frenchman’s outstanding performances led to a €30 million move to Atlético in the summer of 2014.</p>

                <p>In Madrid, Griezmann’s progress led him to be considered as one of the best players in the world. The striker was part of the squad that were runners-up in the Champions League in 2016, a year in which he also finished amongst the top three in the battle for the Ballon d’Or. Internationally, In fact, he was the star of the French team that reached the final of the 2016 European Championship played in France, where he won the Golden Boot as top scorer (six goals). He was also outstanding for France\'s World Cup winning side in 2018 in Russia, where he was the MVP of the final against Croatia. In the same season he was part of Atlético\'s Europe League winning squad. </p>

                <p>The Frenchman can play anywhere in the front three although his usual position is on the left wing. Griezmann is a very mobile player who can score and also create goals. He is also very quick on the counter attack and dangerous with his shots from distance and from set pieces.</p>
                 '
            ],
            [
                'name' => 'Ousmane Dembélé',
                'country' => 'France',
                'date_of_birth' => '15/05/1997',
                'weight' => '67kg',
                'height' => '178cm',
                'club_debut' => '09/09/2017',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/5082b6bd-7cab-4a88-8051-565280311748/11_DEMBELE_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Ousmane Dembélé',
                'position' => 'forward',
                'work_foot' => 'bouth',
                'number' => 11,
                'legend' => 0,
                'content' => '
                <p>At the end of 2014 he signed his first professional contract with French club Rennes. He made his Ligue 1 debut at just the age of 17 and he scored his first goal against Girondins. His progress attracted the attention of Borussia Dortmund and he signed for the Bundesliga club in the summer of 2016. Dembélé adapted quickly to German football, scoring 12 goals and providing 20 assists in his first season.</p>

                <p>He signed a five year deal with FC Barcelona in the summer of 2017. He made his Barça debut on 9 September in the league against Espanyol and celebrated by providing an assist. He scored his first goal as a blaugrana against Chelsea in the Champions League of that season. </p>

                <p>After representing France at various youth levels, he is a current France international and formed part of the 2018 World Cup winning squad in Russia alongside his blaugrana team mate Samuel Umtiti. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>2 La Liga (2017/18, 2018/19)</li>
                      <li>Copa del Rey (2017/18)</li>
                      <li>Spanish Super Cup (2018/19)</li>
                      <li>World Cup (2018)</li>
                      <li>German Cup (2016/17)</li>
                  </ul>
                  '
            ],
            [
                'name' => 'Frenkie de Jong',
                'country' => 'Netherlands',
                'date_of_birth' => '12/05/1997',
                'weight' => '74kg',
                'height' => '180cm',
                'club_debut' => '16/08/2019',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/dbb35ad4-e1dc-43bf-a2d3-15571a51393b/21_D-JONG_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Frenkie de Jong',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 21,
                'legend' => 0,
                'content' => '
                <p>Frenkie de Jong was born in Arkel (Netherlands) on May 12, 1996. The Barça player started his career at Willem II in the Eredivise, the Dutch first division. In 2016 he signed for Ajax, where he gradually gained a place in the starting eleven. His versatility and his great vision allowed him to become the midfield organiser for the team from Amsterdam.</p>

                <p>His effectiveness ensures that he always participates, and his distribution of the ball is second to none.</p>
                 '
            ],
            [
                'name' => 'Ivan Rakitic',
                'country' => 'Switzerland',
                'date_of_birth' => '10/03/1988',
                'weight' => '78kg',
                'height' => '184cm',
                'club_debut' => '24/08/2014',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/c53cab13-4706-4786-b782-7060e3556c67/04_RAKITIC_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Ivan Rakitic',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 4,
                'legend' => 0,
                'content' => '
                <p>Ivan Rakitic joined FC Barcelona in the summer of 2014. The midfielder was born to Croatian parents in Rheinfelden, Switzerland, on March 10, 1988, and he has dual nationality, allowing him to represent Croatia at international level.</p>

                <p>Rakitic, who spent his childhood in Switzerland, followed in his father and brother’s footsteps and began to set his sights on a career in football. He joined the Basel youth team, where he made his debut in a UEFA Cup match against NK Široki Brijeg on September 29 of 2005 at the age of 17. He made his league debut against Neuchatel Xamax later in the same season. However, it wasn’t until the following campaign, 2006/07, that he became a regular starter for the team. He won the Swiss Cup with Basel and he scored 11 goals that season, which earned him the accolade of Best Young Player of the Swiss Superleague.</p>

                <p>His breakout season in Switzerland didn’t go unnoticed, and one of the historic teams in Germany, Schalke 04, signed the player in the summer of 2007. On his Bundesliga debut he scored against VF Stuttgart. He played for Schalke 04 for three and a half seasons, and during this time he scored 17 goals in 135 matches and his team finished second in the 2009/2010 season. In the winter transfer window of the 2010/11 season, Rakitic signed for Sevilla and became the leader of the team’s midfield. Rakitic would go on to win the Europa League that year, and was named the man of the match in the final.</p>

                <p>In his first season at Barça, the ex-Sevilla player played a total of 50 games, scoring a total of eight goals. His most important goal came on 6 June 2015 in the Champions League final against Juventus, opening the scoring and paving the way to Barça\'s fifth Champions League title. Over the next three seasons, from 2015/16 to 2017/18, Rakitic played over 50 games per season, become a mainstay in the midfield. After winning the 2017/18 Copa del Rey, Rakitic holds the distinction of having won every final played with Barça.</p>

                <p>Since his Croatia national team debut at the age of 19, on September 8, 2007, he has become a mainstay. He has played in two Euro Championships (2008 and 2010), in the 2014 and 2018 World Cups, in which he was a key figure.</p>

                <p>Ivan Rakitic made his official debut for the Club on 24 August 2014 in La Liga against Elche. The match ended with a Barça victory (3-0).</p>

                 <h3>Honours</h3>
                   <ul>
                      <li>World Club Cup (2015/16)</li>
                      <li>Champions League (2014/15)</li>
                      <li>4 La Liga (2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>4 Copas del Rey (2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>2 Spanish Super Cups (2016/17, 2018/19)</li>
                      <li>European Super Cup (2015/16)</li>
                      <li>Swiss Cup (2007)</li>
                      <li>Europa League (2013/14)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Sergio Busquets',
                'country' => 'Spain',
                'date_of_birth' => '16/07/1988',
                'weight' => '76kg',
                'height' => '189cm',
                'club_debut' => '13/09/2008',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/d14eb72c-7bed-49b1-9201-16def349fbe4/05_SERGIO_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Sergio Busquets',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 5,
                'legend' => 0,
                'content' => '
                <p>Born in Sabadell, he joined the Barça youth system in the summer of 2005 from Jabac. That season, playing for the U19A side, he won three major trophies. In the 2006-07 season, Sergio Busquets continued with U19A and was one of the core players in the side coached by Àlex Garcia. In his third year at the level, and despite still being a midfielder, Sergio scored seven goals in 26 games.</p>

                <p>The following season, Busquets moved up to Barça B under coach Pep Guardiola, making 23 appeaarances, scoring twice, and making a major contribution to the promotion to Division 2B. He appeared for the first team in the Copa Catalunya matches of 2007-08 and 2008-09, and gradually Busquets would get more and more chances as the season drew on, and was very much involved in the famous treble winning season. He started both the Copa del Rey and Champions League finals, thus ending an incredible twelve months in which he progressed from the third division to being a top-flight, European champion.</p>

                <p>In the 2009-10 season, he established his name as one of the most talented central midfielders on the international scene. In the 2010/11 season, he consolidated his position in midfield as the team won the Spanish Super Cup, the league and the Champions League. In 2011/2012, Sergio Busquets continued his meteoric rise and helped the team to victories in the Spanish Super Cup, European Super Cup, Club World Cup and Copa del Rey, scoring two goals in 52 appearances and establishing a regular place in the first eleven.</p>

                <p>More established as a defensive midfielder, Busquets played 45 games in the 2012/13 season, helping Barça win the La Liga title with 100 points, and reaching the milestone of 200 Barça appearances. In 2013/14, Busquets continued with his midfield dominance and appeared in 48 matches. In 2014/2015, he made it to 300 career games played. That year he played 44 official matches, scoring one goal, the last minute winner against Valencia at Mestalla.</p>

                <p>After seven seasons with the first team, and coinciding with the farewell of Xavi Hernández, Busquets in the summer of 2015 went from being the fourth captain to the third captain. In 2017/18, the superstar midfielder surpassed the 450-game plateau, more than Barça legend Carles Rexach.</p>

                <p>His fine form for Barça has won him international recognition. He played his first game for Spain on April 1, 2009, at the Ali Sami Yen, against Turkey (1-2). He also played in the 2009 Confederations Cup in South Africa. Vicente del Bosque used Busquets as a starting midfielder in the side than won the country’s first World Cup in South Africa in 2010, and he also starred in the successful defence of the European Championship crown in 2012 . Busquets and Spain were runners-up at the 2013 Confederations Cup in Brazil and in Spain’s less successful bid to retain the World Cup in Brazil in 2014. Busquets also played in Euro 2016 in France and the 2018 World Cup in Russia.</p>

                <p>Busquets made his official debut for Barça on 13 September 2008 in La Liga against Racing. The match ended with a draw (1-1).</p>

                <p>Sergio Busquets is the son of Carles Busquets, who played in goal for FC Barcelona in the 1990s.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>3 World Club Cups (2009/10, 2011/12 and 2015/16)</li>
                      <li>3 Champions Leagues (2008/09, 2010/11 and 2014/15)</li>
                      <li>8 La Liga (2008/09, 2009/10, 2010/11, 2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>6 Copas del Rey (2008/09, 2011/12, 2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>6 Spanish Super Cups (2009/10 , 2010/11, 2011/12, 2013/14, 2016/17, 2018/19)</li>
                      <li>3 European Super Cup (2009/10, 2011/12, 2015/16)</li>
                      <li>European Championship (2012)</li>
                      <li>World Cup (2010)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Arthur Melo',
                'country' => 'Brazil',
                'date_of_birth' => '12/08/1996',
                'weight' => '73kg',
                'height' => '171cm',
                'club_debut' => '18/08/2018',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/0febd1af-8209-4b4b-96a4-026383ddab95/08_ARTHUR_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Arthur Melo',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 8,
                'legend' => 0,
                'content' => '
                <p>Born on August 12, 1996 in the Brazilian town of Goiania, Arthur Melo is a young midfielder who stands out for his ability to organize play and his teamwork, as well as for his vertical play and passing skills.   </p>

                <p>Arthur started playing soccer in 2008 in the lower categories of the Goiás Esporte Clube, in his hometown in Brazil. After two seasons, in 2010, he joined the ranks of Grêmio, from Porto Alegre, where he continued his development.</p>

                <p>In 2015, Arthur debuted with the first team under then- Grêmio manager Luiz Felipe Scolari. Since then, the midfielder has been progressing and taking on a leading role, until becoming a key part of the Brazilian club in 2017. That same year, at just 20 years of age, he won the 2017 Copa Libertadores, and was the MVP of the return leg of that year’s final.</p>

                <p>Arthur was presented as an FC Barcelona player on July 12, 2018, with a six-year contract.</p>

                <p>Arthur is a very creative and skilled player with the ball at his feet. His good vision of the game and the passing control allow him to organize play with great ability. He is an attacking midfielder committed to helping out on defense. Good at changing the pace of play and reliable when dribbling up the field, the Brazilian stands out because of his vertical play and his decisiveness with the final pass.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>La Liga (2018/19)</li>
                      <li>Spanish Super Cups (2018/19)</li>
                      <li>Copa América (2019)</li>
                      <li>Campeonato Brasileiro Série A Best Newcomer (2017)</li>
                      <li>Copa do Brasil (2016)</li>
                      <li>Copa Libertadores (2017)</li>
                      <li>Campeonato Gaúcho (2018)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Arturo Vidal',
                'country' => 'Chile',
                'date_of_birth' => '22/05/1987',
                'weight' => '75kg',
                'height' => '180cm',
                'club_debut' => '12/08/2018',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/5d8e23ab-09a8-41ea-86aa-c213f8590688/22_VIDAL_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Arturo Vidal',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 22,
                'legend' => 0,
                'content' => '
                <p>Born in San Joaquín in Santiago, Chile on 22 May 1987, Arturo Vidal is a physical presence, excellent at winning the ball back but also with a talent for getting into the opposition box.</p>

                <p>The Chilean began his professional career in home country with Colo-Colo. At the age of 20 he made the switch to European football with Bayer Leverkusen in the Bundesliga. In 2011 he moved to Juventus where he spent four seasons as well as playing against Barça in the final of the 2015 Champions League. After the defeat against the blaugranes, Vidal packed his bags and headed back to Germany, this time to join Bayern Munich. With the Bavarian giants he played 124 matches, 97 in the starting XI, scoring 22 goals and providing 14 assists. He is also vital member of the Chilean national side, featuring in two World Cups and winning the Copa América in 2015 and 2016.</p>

                <p>He arrived at Barça in the summer of 2018 on a three year contract to strengthen the midfield.</p>

                <p>Arthur was presented as an FC Barcelona player on July 12, 2018, with a six-year contract.</p>

                <p>Arthur is a very creative and skilled player with the ball at his feet. His good vision of the game and the passing control allow him to organize play with great ability. He is an attacking midfielder committed to helping out on defense. Good at changing the pace of play and reliable when dribbling up the field, the Brazilian stands out because of his vertical play and his decisiveness with the final pass.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>La Liga (2018/19)</li>
                      <li>Spanish Super Cups (2018/19)</li>
                      <li>4 Italian Leagues (2011/12, 2012/13, 2013/14, 2014/15)</li>
                      <li>2 Italian Super Cups (2011/12, 2012/13)</li>
                      <li>Italian Cup (2014/15)</li>
                      <li>German League (2015/16. 2016/17, 2017/18)</li>
                      <li>German Super Cup (2016, 2017)</li>

                  </ul>
                '
            ],
            [
                'name' => 'Carles Aleñá',
                'country' => 'Spain',
                'date_of_birth' => '05/01/1998',
                'weight' => '73kg',
                'height' => '180cm',
                'club_debut' => '30/11/2016',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/37159be4-c87c-4273-ac14-e38942b0b034/19_ALENA_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Carles Aleñá',
                'position' => 'midfielder',
                'work_foot' => 'right',
                'number' => 19,
                'legend' => 0,
                'content' => '
                <p>Carles Aleñá has been an FC Barcelona player since the age of eight. His natural ability has seen him rise quickly through the youth teams to reach Barça B. When he was a still officially an U19A player, he was already a fixture in the B squad.</p>

                <p>On 30 November 2016 he made his debut for Luis Enrique\'s first team in the Copa del Rey tie away at Hércules which ended 1-1. The midfielder from Matatró scored Barça\'s only goal of the game with an excellent effort from outside the box.</p>

                <p>On 6 September 2017, Aleñá signed a contract extension with FC Barcelona for the next three seasons with an option for two more should he reach the first team. For the 2018/19 season, Aleñá was confirmed as part of the FC Barcelona first team squad. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>6 La Ligas (2010/11, 2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>Spanish Super Cups (2018/19)</li>
                      <li>2 Copas del Rey (2016/17, 2017/18)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Sergi Roberto',
                'country' => 'Spain',
                'date_of_birth' => '07/02/1992',
                'weight' => '68kg',
                'height' => '178cm',
                'club_debut' => '10/11/2010',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/30840020-0e4c-4e76-8261-f63c9b8a0356/20_S-ROBERTO_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Sergi Roberto',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 20,
                'legend' => 0,
                'content' => '
                <p>When Sergi Roberto was 14 years old he traded the town of Reus for La Masia. The ex-Nàstic Tarragona midfielder improved year after year until he became a crucial player for Luis Enrique’s Barça B team.</p>

                <p>He continued to make the occasional appearance for the first team when required for the next two years, and was eventually promoted full-time to Gerardo Martino’s team in the summer of 2013, with the number 24 on his back. Under the Argentine coach, he played 27 matches, 17 in the league, 6 in the Copa del Rey and 4 in the Champions League.</p>

                <p>The following year, now under Luis Enrique, Sergi Roberto appeared in 18 matches, 12 in the league, 2 in the Champions League and 4 in the Copa del Rey. His two goals that season were both scored in the Copa del Rey. </p>

                <p>Sergi Roberto made his official debut for Barça on 10 November 2010 in a 5-1 Copa del Rey victory over Ceuta at Camp Nou. The versatile midfielder is now an experienced member of the season and in the 2017/18 he extended his contract with the blaugranes. He can play in midfield or at right back and he will be part of the Club\'s history thanks to his historic sixth goal in the famous 6-1 comeback victory over PSG in the 2016/17 Champions League. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>2 World Club Cups (2011/12 and 2015/16)</li>
                      <li>2 Champions Leagues (2010/11 and 2014/15)</li>
                      <li>6 La Liga (2010/11, 2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>5 Copas del Rey (2011/12, 2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>5 Spanish Super Cups (2010/11, 2011/12, 2013/14, 2016/17, 2018/19)</li>
                      <li>2 European Super Cup (2011/12, 2015/16)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Samuel Umtiti',
                'country' => 'Cameroon',
                'date_of_birth' => '14/11/1993',
                'weight' => '75kg',
                'height' => '182cm',
                'club_debut' => '17/08/2016',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/245575de-7793-4a7f-8db0-6ab58dde5605/23_UMITITI_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Samuel Umtiti',
                'position' => 'midfielder',
                'work_foot' => 'left',
                'number' => 23,
                'legend' => 0,
                'content' => '
                <p>Samuel Umtiti came to FC Barcelona to reinforce the team\'s defensive line. The Cameroon-born Frenchman landed in Barcelona on 14 July 2016 from Olympique Lyonnais, where he was a regular in the starting eleven.</p>

                <p>After emerging from the youth ranks of Lyon, Umtiti reached the first team in the 2011/12 season. There he played a total of 170 games, scoring five goals and handing out two assists.</p>

                <p>Samuel Umtiti was officially presented as an FC Barcelona player on 15 July 2016. Since his first season with the Club, Umtiti has become a mainstay on the Barça defence. He has played 83 official games in his two seasons on the team. His importance, thus, was reaffirmed on 3 June 2018, when he was renewed through the 2022/23 season. </p>

                <p>Before debuting with France, Umtiti had already risen through all of the lower national team categories. Umtiti was summoned to the French national team for Euro 2016, in which France were the tournament\'s hosts. Umtiti debuted in the Euro quarter-finals against Iceland. His great performance in his first game with France put him in the starting line-up for the rest of the competition. Umtiti went on to play every minute in the semi-final against Germany and in the final against Portugal. Umtiti won the World Cup with France during his first time playing in the tournament. In Russia in 2018, he was a key piece for France, starting six of their seven games en route to the title. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>World Cup(2018)</li>
                      <li>2 La Liga (2017/18, 2018/19)</li>
                      <li>2 Copas del Rey (2016/17, 2017/18)</li>
                      <li>2 Spanish Super Cups (2016/17, 2018/19)</li>
                      <li>French Cup (2011/12)</li>
                      <li>French Super Cup (2012)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Jordi Alba',
                'country' => 'Spain',
                'date_of_birth' => '21/03/1989',
                'weight' => '68kg',
                'height' => '170cm',
                'club_debut' => '19/08/2012',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/1a1d94cf-8b38-46d1-9dca-9b42cd6eabcf/18_JORDI-ALBA_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Jordi Alba',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 18,
                'legend' => 0,
                'content' => '
                <p>Jordi Alba signed for FC Barcelona on 5 July 2012 after the Club had reached an agreement with Valencia for his transfer. The Catalan defender was 23 when he joined the Club and was the first signing of the 2012/13 season.</p>

                <p>Jordi Alba i Ramos was born in Hospitalet de Llobregat on 21 March 1989. After joining Barça at the age of 10, he proceeded through the various academy teams, alongside players such as Bojan Krkic, Fran Mérida and Dos Santos.</p>

                <p>At the end of the 2007 season, he left Barça and joined UE Cornellà, where he spent a season, before joining Valencia, who paid a €6,000 signing on fee for him. He figured for the U19A team and then València CF Mestalla, where he was a key piece in the team which won promotion to the Second Disivison B. During this period, he also won a debut call up for the Spain U19 team. </p>

                <p> He spent the 2008/09 season on loan at Gimnàstic de Tarragona, playing 30 games in the Second Division and played for the Spanish U21 team and figured in the Under-20 World Cup in Egypt. He returned to Valencia and became a first team squad member that summer. </p>

                <p>He made his league debut on 13 September 2009 against Real Valladolid. Although he had been signed as a left back, he showed his versatility by figuring in various positions and before leaving for Barça, he played 108 times for Valencia – including 48 matches in his last season.</p>

                <p>On 5 July 2012 he was unveiled as a Barça player. Jordi Alba made his official debut for the Club on 19 August 2012 in La Liga against Real Sociedad in a game that ended with a 5-1 win for the blaugranes. Tito Villanova’s first signing of the 2012/13 season. He was a first team regular from the start and in 44 official games in his debut season he scored five goals as the team marched to the Liga title with 100 points.</p>

                <p>He had less of a presence in the 2013/14 season due to a series of injuries and only appeared in 26 games. After three months on the sidelines he was back for the cup match with Cartagena, but shortly after hurt his hamstring and was out of action once again.</p>

                <p>In his third season, Jordi Alba made it to 100 appearances for the club on 28 February 2015 away to Granada. He played 27 games in La Liga, 11 in the Champions League and 5 in the cup, scoring two goals. On June 8, he renewed his contract until 30 June 2020.</p>

                <p>His fourth season at the Club (2015/16) saw him make 45 appearances for the blaugranes and the following season he made his 200th offical appearance on 16 August 2017. </p>

                <p>His great performances at Valencia won him a call up to the full Spanish squad in September 2011 and he made his debut under coach Vicente Del Bosque on 11 October 2011 against Scotland. In the summer of 2012 he was one of the stars of Spain\'s win in the European Championships played in Poland and Ukraine. The following year he played in the Confederations Cup final and in 2014 he was part of the Spain squad that took part in the World Cup in Brazil. Alba also featured in the Spain squad for the 2018 World Cup in Russia. </p>

                <h3>Honours</h3>
                   <ul>
                      <li>World Club Cup (2015/16)</li>
                      <li>Champions League (2014/15)</li>
                      <li>5 La Liga (2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>4 Copas del Rey (2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>European Super Cup (2015/16)</li>
                      <li>3 Spanish Super Cups (2013/14, 2016/17, 2018/19)</li>
                      <li>European Championship (2012)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Gerard Piqué',
                'country' => 'Spain',
                'date_of_birth' => '02/02/1987',
                'weight' => '85kg',
                'height' => '194cm',
                'club_debut' => '13/08/2008',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/14bea8c3-8471-4b26-a71b-3697b0650e53/03_PIQUE_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Gerard Piqué',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 3,
                'legend' => 0,
                'content' => '
                <p>Gerard Piqué returned to FC Barcelona, the club where he started out as a youngster, in the summer of 2008, after three seasons with Manchester United and one on loan with Real Zaragoza.                 </p>

                <p>Gerard Piqué was born on 2 February 1987 and since that day he has been a club member of FC Barcelona. Since he was very young he has been involved with the club and when he was 10 years old he joined the youngest boys\' team at the club at that time (Under-12 B).</p>

                <p>From then on he passed through the various youth levels, picking up titles along the way and winning plaudits in all age categories (Alevín A, Infantil B and A, Cadete B and A and Juvenil B and A). When playing at Cadet B level Tito Vilanova was his manager, who he would coincide with again when he eventually made it into the first team. In all this time of development and learning, Gerard Piqué showed a great ability to adapt to different positions on the field and gained a reputation for goal scoring, despite his defensive position.</p>

                <p> At the end of the 2003/04 season, Gerard Piqué took a break from his life at FC Barcelona and left for Manchester United. He was loaned out to Real Zaragoza for the 2006/07 season. The 2007/08 season saw Piqué return to Manchester United, where he was able to celebrate the Premiership and Champions League double. </p>

                <p>In his first season with Barça, Piqué soon showed why he had been signed. He became a first choice centre back and also scored three goals, one in each competition, totalling 45 appearances. In his second year he consolidated his reputation as one of the best central defenders in the world, forming a regular partnership with Puyol and winning his second league title with the Club. In the third year, Piqué became an even more important part of the centre of the defence and faced his former side Manchester United in the final of the Champions League, which FC Barcelona won. He made his 100th appearance for Barça on September 22, 2010 against Sporting Gijón.</p>

                <p>In the 2012/13 season, he surpassed 200 games for Barça as the team marched to the title with 100 points. In 2013/14, Piqué was involved in 39 official matches and scored four goals. Piqué also had the honour of scoring Barcelona’s 1000th international goal against Celtic in the Champions League group stage. In 2014/15, he played 43 games for the team as they marched to a second historic treble. Over the next three seasons, Piqué scored 12 goals and made 136 appearances. </p>

                <p>As for his international experience, Piqué played in the 2007 U-20 World Cup in Canada, the 2006 U19 European Championships in Poland (which Spain won) and the 2004 U17 European Championships in France (which Spain finished as runners-up). In 2009, he made his international debut in the Confederations Cup in South Africa. He has since become a permanent feature of Spain\'s defence, playing every minute of the country’s first ever World Cup win in 2010, and also playing a key role in the conquest of the 2012 European Championship. Piqué also played in the 2013 Confederations Cup and 2014 World Cup in Brazil, the Euro 2016, and the 2018 World Cup in Russia.</p>

                <p>Gerard Piqué made his official debut for the Club on 13 August 2008 in the Champions League against Wisła Kraków. The match ended in a comfortable win for the blaugranes (4-0).</p>

                <h3>Honours</h3>
                   <ul>
                      <li>3 World Club Cup (2009/10, 2011/12, 2015/16)</li>
                      <li>3 Champions Leagues (2008/09, 2010/11, 2014/15)</li>
                      <li>8 La Liga (2008/09, 2009/10, 2010/11, 2012/13, 2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>6 Copas del Rey (2008/09, 2011/12, 2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>3 European Super Cups (2009/10, 2011/12, 2015/16)</li>
                      <li>6 Spanish Super Cups (2009/10, 2010/11, 2011/12, 2013/14, 2016/17, 2018/19)</li>
                      <li>European Championship (2012)</li>
                      <li>World Cup (2010)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Clément Lenglet',
                'country' => 'France',
                'date_of_birth' => '17/06/1995',
                'weight' => '81kg',
                'height' => '186cm',
                'club_debut' => '12/08/2018',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/a7a88778-e4d0-49da-bf98-71a84c9cdb0c/15_LENGLET_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Clément Lenglet',
                'position' => 'defender',
                'work_foot' => 'left',
                'number' => 15,
                'legend' => 0,
                'content' => '
                <p>Born on June 17, 1995 in Beauvais, France, Clément Lenglet is one of the most promising defenders on the planet. The central defender started his career with Nancy in the French Second Division, making his debut in 2013. He developed at the club and despite his youth, he became a vital part of the team. In the 2015/16 Lenglet was key in Nancy winning the French Second Division title. </p>

                <p>His excellent performances in France alerted the interest of Sevilla and in January 2017, the Andalusian club signed the defender. Lenglet settled in quickly, his elegant style and his commitment making him a key component of the squad. The Frenchman moved on to the next level and developed into a top class centre back.</p>

                <p>On 13 July 2018 he was presented as an FC Barcelona player with a contract until 30 June 2023.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>La Liga (2018/19)</li>
                      <li>Spanish Super Cups (2018/19)</li>
                      <li>Ligue 2 Championship (2015/16)</li>
                  </ul>
                '
            ],
            [
                'name' => 'Nélson Semedo',
                'country' => 'Portugal',
                'date_of_birth' => '16/11/1993',
                'weight' => '67kg',
                'height' => '177cm',
                'club_debut' => '16/08/2017',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/9707f5b9-676a-4ab3-94f5-162a04f89998/02_SEMEDO_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => '\'Nélson Semedo',
                'position' => 'goalkeeper',
                'work_foot' => 'right',
                'number' => 2,
                'legend' => 0,
                'content' => '
                <p>Nélson Cabral Semedo, born 16 November 1993, joined Benfica in 2012 from Sintrense. He went back on loan to his former club for a year before becoming a full time member of the Eagles’ reserves. He performed brilliantly for the second team, and made his first team debut in the 2015 Portuguese Super Cup against Sporting Lisbon.</p>
                <p>In two seasons with the first team, Semedo confirmed his early promise, making 63 appearances, including eleven in the Champions League. At Portugal’s most successful club he won two leagues, one cup, one league cup and one super cup.</p>
                <p>On 14 July 2017 he was presented as a new FC Barcelona player. In his first season with Barça, Semedo made 35 official appearances. Hedebuted on 16 August 2016 in El Clásico, precisely the return leg of the Spanish Super Cup.</p>
                <p>He made his full international debut for Portugal on 11 October 2015 and was in the squad that finished third at the 2017 Confederations Cup in Russia, playing in two of his country’s games.</p>
                '
            ],
            [
                'name' => 'Jean-Clair Todibo',
                'country' => 'Cayenne',
                'date_of_birth' => '30/12/2000',
                'weight' => '81kg',
                'height' => '190cm',
                'club_debut' => null,
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/cf8a1d1a-eb46-436a-84a1-b5b4583f4381/06_TODIBO_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Jean-Clair Todibo',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 6,
                'legend' => 0,
                'content' => '
                <p> Jean-Clair Todibo was born in Cayenne, the capital of French Guiana in South America, on 30 December 1999. The Frenchman played for the FC Les Lilas club at the youth level before joining Toulouse FC in 2016. In total he made 10 appearances for the French club all of them in the starting XI and scored one goal. Furthermore, the Frenchman is excellent at bringing the ball out from the back and is also outstanding in the air.</p>

                <p>On 8 January 2019 it was announced that the Club had reached an agreement for him to join the Club on 1 July. However, on 31 January FC Barcelona and Toulouse reached an agreement for him to join the blaugranes with immediately. On 1 February he was presented as a new FC Barcelona player.</p>
                '
            ],
            [
                'name' => 'Moussa Wague',
                'country' => 'Bignona',
                'date_of_birth' => '04/10/1998',
                'weight' => '70kg',
                'height' => '177cm',
                'club_debut' => null,
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/069170d4-1f84-4388-8d67-2c0cdf503117/16_WAGUE_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Moussa Wague',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 16,
                'legend' => 0,
                'content' => '
                <p>Moussa Wague is a former pupil of Aspire Academy, a Middle Eastern football school where he honed his skills from 2014 to 2016. His quality and level of performance with the junior categories of the Senegal national team saw him signed by KAS Eupen in November 2016. After 43 matches for the Belgian team, he joined Barça B in the summer of 2018, just after the World Cup. He became the youngest African to score in the competition, in Senegal’s game against Japan.</p>

                <p>In that same season Wague began to make appearances for the first team. Coach Ernesto Valverde regularly included him in first team training sessions and he eventually made his debut in a league game against Huesca away from home. The defender made two more appearances for the first team against Celta and Eibar in the 2018/19 season before joining the first team squad ahead of the 2019/20 campaign. </p>

                <p>His excellent season led him to being included in the Senegal squad that was runner up in the 2019 Africa Cup of Nations. </p>
                '
            ],
            [
                'name' => 'Junior Firpo',
                'country' => 'Dominican Republic',
                'date_of_birth' => '22/08/1996',
                'weight' => '78kg',
                'height' => '184cm',
                'club_debut' => '25/08/2019',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/0e50956d-d3d4-4bd9-b152-93468c5cd1b5/24_JUNIOR_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Junior Firpo',
                'position' => 'defender',
                'work_foot' => 'right',
                'number' => 24,
                'legend' => 0,
                'content' => '
                <p>Born in Santo Domingo in the Dominican Republic on 22 August 1996, Junior Firpo arrived in Malaga in Spain when he was 6 years old. There, he would play for several local clubs at youth level, such as Atlético Benamiel and Puerto Malagueño. In the summer of 2014 he joined Real Betis, playing for their youth teams before making his debut for the first team on 12 February, 2018, in a 0-1 win over Deportivo La Coruña. He would play almost every remaining league game that season, and was similarly prolific during the 2018/19 campaign before signing for Barça in the summer of 2019. Although he was born in the Dominican Republic, he has played for Spain at Under-21 level -including winning the 2019 European Championships.</p>

                <p>His height, speed and precision make him one of the most promising left backs in LaLiga. One of the young talents who stood out the most in 2018/19, he also causes problems for the opposition when attacking on the wing and in the box.</p>

                <p>A tireless worker able to get back in position quickly, Junior Firpo is also adept at the possession football and passing play that is fundamental to FC Barcelona\'s style. </p>
                '
            ],
            [
                'name' => 'Norberto Murara',
                'country' => 'Brazil',
                'date_of_birth' => '19/07/1989',
                'club_debut' => null,
                'weight' => '84kg',
                'height' => '190cm',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/55aa2a9d-0b32-4406-83a8-c0083d353dc0/13_NETO_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Norberto Murara',
                'position' => 'goalkeeper',
                'work_foot' => 'right',
                'number' => 1,
                'legend' => 0,
                'content' => '
                <p>Norberto Murara \'Neto\' (Araxá, July 19, 1989) has speed and movement that has allowed him to defend his goal with security and deliver outstanding performances for Valencia (2017-2019), Juventus (2015-2017), Fiorentina (2011 -2015) and Paranaense (2002-2011). A goalkeeper from a very young age in his homeland, he has emulated his father.</p>
                <p>Neto\'s last two seasons at Fiorentina were spectacular, helping him to secure a move to Juventus, however, the entire time he was in Turin was as Buffon\'s substitute. His character, and a continuous improvement year after year, led the Brazilian to Valencia where he has spent the last two years.</p>
                <p>The Brazilian has a real presence between the posts. His height, almost two metres, combined with his agility makes Neto a goalkeeper that\'s very difficult for a forward to score against. Excellent in the air when required, he\'s difficult to beat on the goal line and in a one against one, and, despite his stature, Neto has excellent speed and reflexes.</p>
                '
            ],
            [
                'name' => 'Marc-André ter Stegen',
                'country' => 'German',
                'date_of_birth' => '30/04/1992',
                'weight' => '85kg',
                'height' => '187cm',
                'club_debut' => '17/09/2014',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/09/26/768dab0d-64f4-4259-8a21-581d0dcdc89d/01_TER-STEGEN_JUGADORS-WEB.jpg?width=840&height=525',
                'img_alt' => 'Marc-André ter Stegen',
                'position' => 'goalkeeper',
                'work_foot' => 'right',
                'number' => 13,
                'legend' => 0,
                'content' => '
                <p>His only previous club was Borussia Mönchengladbach, which he joined in 1996 at the age of just 4, and by the time he was 18 he had already made his first team debut on 10 April 2011 in the Bundesliga against FC Köln. Mönchengladbach won 5-1, and following a fine performance Ter Stegen was soon the regular first choice keeper, conceding just three goals in his first six outings.</p>

                <p>In the next two games, Ter Stegen and Mönchengladbach faced a relegation playoff, and won 2-1 on aggregate to stay in the top division. The following season, Ter Stegen was named best goalkeeper in the Bundesliga and he went on to complete four seasons and 108 matches in the German league.</p>

                <p>Ter Stegen made his official debut for the Club on 17 September 2014 in Champions League against APOEL Nicosia FC. The match ended with a Barça victory (1-0 ). In his debut season, 2014/15, the goalkeeper played every game in the Champions League and had a leading role in the Copa del Rey, both of which Barça won. Luis Enrique continued going with Ter Stegen in both tournaments the following season, in which he played 27 games. He became the team\'s number one goalkeeper in 2016/17, starting in La Liga and the Champions League, bringing his appearances total with Barça to 141. </p>

                <p>His leading role with Barça gave him a ticket to his first World Cup, in Russia 2018. With Germany, Marc-André ter Stegen made his international debut in 2012, but at the last minute was left out of the World Cup winning squad at Brazil 2014. Aged just 22 years he became the second German goalkeeper at FC Barcelona, following on from Robert Enke.</p>

                <h3>Honours</h3>
                   <ul>
                      <li>World Club Cup (2015/16)</li>
                      <li>Champions League (2014/15)</li>
                      <li>European Super Cup (2015/16)</li>
                      <li>4 La Liga (2014/15, 2015/16, 2017/18, 2018/19)</li>
                      <li>4 Copas del Rey (2014/15, 2015/16, 2016/17, 2017/18)</li>
                      <li>2 Spanish Super Cups (2016/17, 2018/19)</li>
                      <li>Confederations Cup (2017)</li>
                      <li>European Under-19 Championship (2009)</li>
                  </ul>
                  '
            ],

            //legends
            [
                'name' => 'Andrés Iniesta',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/09/21/3461628c-e62c-4570-8996-1e7592ee55fa/39537642.jpg',
                'img_alt' => 'Andrés Iniesta',
                'date_of_birth' => '11/05/1984',
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'midfielder',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Iniesta (Fuentealbilla, Albacete, 1984) was one of the most loved and emblematic of players for the Barça fans. The ‘culers’ has always valued his silky style of play, his commitment to the team and his modesty and good humour off the pitch.</p>

                <p>A creative midfielder of extraordinary footballing quality, Iniesta stood out for his pure talent and his intelligence. Small in stature and elusive, his incredible skills on the ball made him a vital part of the blaugrana system. It is not for nothing that he is considered one of the greatest Spanish players of all time.</p>

                <p>He came to the Club in 1996/97 to join the U14 squad after having played at previous youth levels at his home town club of Albacete: at 12 years of age, everyone who saw him play was immediately aware of what a talent he was. After moving up through the U16 and U19 sides, Iniesta made his debut for the first team under coach Louis Van Gaal on 29 October 2002 while he was still a member of the Barça B squad. His first appearance came in a 1-0 victory in the Champions League away at Club Brugge in Belgium.</p>

                <p>On Since then, Iniesta future as a player was forever linked to that glorious period in which Barça won trophy after trophy whilst at the same time dazzling the fans with the football they played under Frank Rijkaard, Pep Guardiola, Tito Vilanova, Luis Enrique and Ernesto Valverde. It was a remarkable time for the blaugranes with the calming presence of Iniesta in the midfield a vital part of that success. His last minute equaliser against Chelsea at Stamford Bridge that gave Barça a place in the 2009 Champions League final will forever form part of the Club’s history.</p>

                <p>He left the Club on a high having completed the league and Copa del Rey double in the 2017/18 season. In his time at Barça Andrés Iniesta claimed no less than 32 trophies as a blaugrana and the man from Fuentealbilla will always have a place in the heart of every Barça fan. As the former Spain national team coach Vicente del Bosque said, “Iniesta will always be an example of how to play and how to behave as a professional.”</p>

                <p>Iniesta has a street named after him and a statue in his home town of Fuentealbilla to honour his goal in the 2010 World Cup final that gave Spain their first ever win in the competition. Furthermore, a Barça supporters’ club has been created that bears his name.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 2002-2018</li>
                    <li>Games played: 758</li>
                    <li>Goals scored: 66</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>3 World Club Cups (2009/10, 2011/12 and 2015/16)</li>
                    <li>4 Champions League (2005/06, 2008/09, 2010/11 and 2014/15)</li>
                    <li>3 European Super Cups  (2009/10, 2011/12 and 2015/16)</li>
                    <li>9 Leagues (2004/05, 2005/06, 2008/09, 2009/10, 2010/11, 2012/13, 2014/15, 2015/16 and 2017/18)</li>
                    <li>6 Copas del Rey (2008/09, 2011/12, 2014/15, 2015/16, 2016/17 and 2017/18)</li>
                    <li>7 Spanish Super Cups (2005/06, 2006/07, 2009/10, 2010/11, 2011/12, 2013/14 and 2016/17)</li>
                    <li>5 Copas Catalunya (2003/04, 2004/05, 2006/07, 2012/13 and 2013/14)</li>
                    <li>2 Catalan Super Cups (2014/15 and 2017/18)</li>
                </ul>
                '
            ],
            [
                'name' => 'Daniel Alves',
                'country' => 'Brazil',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/e373fbfa-02e3-41e2-badc-48d59df458f1/Dani-Alves.jpg',
                'img_alt' => 'Daniel Alves',
                'date_of_birth' => '06/05/1983',
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'defender',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Dani Alves (Juazeiro, Brazil, 1983) is considered the best right back in Barça’s long history. He arrived in 2008 from fellow La Liga club Sevilla, already with a reputation as one of the best defenders in the country</p>

                <p>After five successful seasons with Sevilla, Dani Alves came to FC Barcelona in the summer of 2008. His physical strength earned him the nickname The Tarantula in his native Brazil and he was soon the indisputable first choice at right back for the blaugranes. In his eight seasons in a Barça shirt he made 414 appearance, 391 of which came in official competition, for an average of 49 games a season. With his solidity in defence, his rampaging runs down the flank, and his potent shot from outside the box, he was a vital component for four different blaugrana coaches: Pep Guardiola, Tito Vilanova, Tata Martino and Luis Enrique. Furthermore, his commitment to the team and his extrovert personality made him a firm favourite with the Barça fans and one of the stars when it came to celebrating the team’s many successes.</p>

                <p>At the time of his departure in the summer of 2016, Dani Alves was second on Barça\'s all-time appearance list for foreign players, behind none other than Leo Messi. Also, he is one of the players with the most trophies having won 26 with Barça, five with Sevilla and three with the Brazilian national team. Football fans will never forget his raids down the right hand side in tandem with Messi, wreaking havoc on opposing defences.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at Barça: 2008-2016</li>
                    <li>Games: 414</li>
                    <li>Goals: 23</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>3 World Club Cups (2009/10, 2011/12 and 2015/16)</li>
                    <li>3 Champions Leagues (2008/09, 2010/11 and 2014/15)</li>
                    <li>3 European Super Cups  (2009/10, 2011/12 and 2015/16)</li>
                    <li>6 Leagues (2008/09, 2009/10, 2010/11, 2012/13, 2014/15 and 2015/16)</li>
                    <li>4 Copas del Rey (2008/09, 2011/12, 2014/15 and 2015/16)</li>
                    <li>4 Spanish Super Cups (2009/10, 2010/11, 2011/12 and 2013/14)</li>
                    <li>2 Copas Catalunya (2012/13 and 2013/14)</li>
                    <li>1 Catalan Super Cup (2014/15)</li>
                </ul>
                '
            ],
            [
                'name' => 'Xavi Hernández',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/dd507edf-a7e4-4ddf-9305-fbbf73e5b2f2/4691622.jpg',
                'img_alt' => 'Xavi Hernández',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'midfielder',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Xavi (Terrassa, Barcelona, 1980) is, statistically speaking, the most successful player in Spanish football history. A midfielder from the club’s youth system, he’s the perfect example of the Barça values</p>

                <p>Xavi (Terrassa, Barcelona, 1980) is, statistically speaking, the most successful player in Spanish football history. A midfielder from the club’s youth system, he’s the perfect example of the Barça values. The conductor of the FC Barcelona orchestra reads the game to absolute perfection. He always seemed to pick out the best-positioned players and had an uncanny knack for creating time and space.</p>

                <p> He joined the U12 team in 1991/92, playing as a traditional centre back, but he would gradually move up the field to the central midfield role in which he became a legend, not just at FC Barcelona but in global football as a whole.</p>

                <p>His mentor was Pep Guardiola and he’ would go on to become the midfield general’s successor. Xavi was astonishingly creative and was the veritable brain in the Barça engine. Though not physically strong, he used his wily knowledge to outwit opponents time and time again. There was no safer guarantee in the middle of the park than this exquisite player.</p>

                <p>He made his first team debut in 1998 under Louis van Gaal, and held onto his first team place for a remarkable seventeen seasons. It was under Frank Rijkaard in 2003 that he really started making his name, and by the time he was playing under his role model Pep Guardiola in the 2008/09 season, his legend was already set in stone. in his final season as a blaugrana. following the retirement of Carles Puyol, he became Barça captain. </p>

                <p>He left Barça in 2015 with full honours, having broken the club record for first team appearances and won more titles than any other Spaniard before him. Xavi will always be part of the FC Barcelona heritage.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1998-2015</li>
                    <li>Games: 869</li>
                    <li>Goals: 97</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 World Club Cups (2009/10, 2011/12)</li>
                    <li>4 Champions Leagues (2005/06, 2008/09, 2010/11 and 2014/15)</li>
                    <li>2 European Super Cups  (2009/10, 2011/12)</li>
                    <li>8 Leagues (1998/99, 2004/05, 2008/09, 2009/10, 2010/11, 2012/13, 2014/15 and 2015/16)</li>
                    <li>3 Copas del Rey (2008/09, 2011/12, 2014/15)</li>
                    <li>6 Spanish Super Cups (2005/06, 2006/07, 2009/10, 2010/11, 2011/12 and 2013/14)</li>
                    <li>6 Copas Catalunya (1999/2000, 2003/04, 2004/05, 2006/07, 2012/13 and 2013/14)</li>
                    <li>Catalan Super Cup (2014/15)</li>
                </ul>
                '
            ],
            [
                'name' => 'Victor Valdés',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/35c5bf00-41e9-4f9b-ae5d-3af2a8be55b8/4696525.jpg',
                'img_alt' => 'Victor Valdés',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'goalkeeper',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p> Valdés (l’Hospitalet de Llobregat, Barcelona, 1982) was Barça’s first choice keeper for 12 seasons</p>

                <p> Valdés was always a fine guarantee between the posts, good with both his hands and feet and an authoritative presence in the area. For many seasons, he was an absolute unconditional between the nets and a huge favourite with the fans.</p>

                <p> The Catalan was excellent at reading the opposition, and was especially cool in one-on-one situations, especially considering that he’d have to make relatively few interventions during many games. He was the ideal goalkeeper for a big team like Barça.</p>

                <p>Valdés had it all: security, agility, reflexes, cold blood, courage, intuition, authority and vision.</p>

                <p> He was promoted up from Barça B in August 2002, but spent his first year alternating in the number one role with Roberto Bonano of Argentina. </p>

                <p>But the following season he became the regular first choice and things would stay that way until 26 March 2014, the day he suffered a serious injury that meant he sat out the rest of what he had already announced was going to be his final season at the club.</p>

                <p>His stats speak for themselves: he donned the FC Barcelona gloves more than any keeper before him, and also won more titles. In 539 official matches he conceded just 442 goals and won the Zamora Trophy for the best goalkeeping record in the league five times (2004/05, 2008/09, 2009/10, 2010/11 and 2011/12), a record only matched by another FCB legend, Antoni Ramallets.</p>

                <p>In 2011/12, he also set a club record by going unbeaten in an astonishing 896 minutes of official football.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 2002-14</li>
                    <li>Games: 602</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 World Club Cups (2009/10, 2011/12)</li>
                    <li>3 Champions Leagues (2005/06, 2008/09, 2010/11)</li>
                    <li>2 European Super Cups  (2009/10, 2011/12)</li>
                    <li>6 Leagues (1998/99, 2004/05, 2008/09, 2009/10, 2010/11, 2012/13)</li>
                    <li>2 Copas del Rey (2008/09, 2011/12)</li>
                    <li>6 Spanish Super Cups (2005/06, 2006/07, 2009/10, 2010/11, 2011/12 and 2013/14)</li>
                    <li>5 Copas Catalunya (2003/04, 2004/05, 2006/07, 2012/13 and 2013/14)</li>
                </ul>
                '
            ],
            [
                'name' => 'Carles Puyol',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/08fa9043-99d6-4c15-94d2-99a420b60419/3965444.jpg',
                'img_alt' => 'Carles Puyol',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'defender',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Puyol (La Pobla de Segur, Lleida, 1978) has been one of the most important players in Barça’s history</p>

                <p>Carles Puyol carried out his entire professional career at Barça but his beginnings were with his local side La Pobla de Segur. In his early years he showed great promise and in 1995 when he was 17 he joined the FC Barcelona youth system and moved into La Masia. He very quickly became a regular at the back for Barça B and on 2 October 1999 he made his long awaited debut for the first team.</p>

                <p> From that moment on, Puyol began to make regular appearances in the first team squad, often initially starting at right back. Thanks to his versatility and his appetite for hard work, he also adapted to playing at centre half, the position in which he would eventually establish himself both at Barça and in the national side.</p>

                <p>Puyol had to wait until the season 2004/05 to win his first major trophy with the balugranes. He was the club captain and a key member of Frank Rijkaard’s side that claimed the league title that season, Puyol holding aloft the trophy in front of the Camp Nou crowd. Success continued as the following season with Barça retaining their league title and claiming the Champions League title in Paris. During Josep Guardiola’s time as coach Puyol continued to play a vital role in the Barça back four before injury finally caught up with him in his final two campaigns as a player. When he finally decided to hang up his boots, only Xavi Hernández had played more games for the Club in official competition.</p>

                <p>Aside from trophies and individual accolade, Carles Puyol’s career will also be remembered for iconic gestures. Two in particular stand out in the memory: kissing the captain’s armband during the historic 6-2 victory in 2009 at the Santiago Bernabéu and allowing Éric Abidal to lift up the Champions League trophy in 2011 at Wembley.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1999-2014</li>
                    <li>Games: 663</li>
                    <li>Goals: 23</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 World Club Cups (2009/10, 2011/12)</li>
                    <li>3 Champions Leagues (2005/06, 2008/09, 2010/11)</li>
                    <li>2 European Super Cups  (2009/10, 2011/12)</li>
                    <li>6 Leagues (1998/99, 2004/05, 2008/09, 2009/10, 2010/11, 2012/13)</li>
                    <li>2 Copas del Rey (2008/09, 2011/12)</li>
                    <li>6 Spanish Super Cups (2005/06, 2006/07, 2009/10, 2010/11, 2011/12 and 2013/14)</li>
                    <li>6 Copas Catalunya (1999/2000, 2003/04, 2004/05, 2006/07, 2012/13 and 2013/14)</li>
                </ul>
                '
            ],
            [
                'name' => 'Samuel Eto\'o',
                'country' => 'Cameroon',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/9468c1a1-2883-4202-b2eb-f4d7691f3ff6/4665030.jpg',
                'img_alt' => 'Samuel Eto\'o',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'forward',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Eto’o (Nkon, Cameroon, 1981) goes down in Barça history as “the indomitable lion”, the forward who never gave up. For 5 years he was the team’s target man, and the host of titles won were, to a massive extent, thanks to the crucial goals he scored</p>

                <p>He was on target in both the Champions League finals of Paris 2006 and Rome 2009.</p>

                <p>Eto’o (Nkon, Cameroon, 1981) signed for Barça in August 2004 from Mallorca after a long tug of war with Real Madrid, who held the rights to the player. It was not long before it became evident just what an astute buy he was, as he became a nightmare for opposing goalkeepers. A fighter until the end, and a man for whom goalscoring was an art that ran in his blood.</p>

                <p>Thanks to assists from the likes of Deco, Ronaldinho, Iniesta, Xavi and Messi, the African scored 152 goals in his 234 games in a Barça jersey. An astounding figure that makes him one of the most prolific strikers in club history. His 108 goals in 144 league outings make him one of the highest league goalscorer on the club records.</p>

                <p>With 131 goals in 201 games, he is also ranked amongst the best in official matches. He was league top scorer in 2004/05 (with 25 goals, hence tied with Forlán) and 2005/06 (with 26 goals).</p>

                <p>An idol in Africa, he was named best player on the continent 2003, 2004, 2005 and 2010. </p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 2004-2009</li>
                    <li>Games: 234</li>
                    <li>Goals: 152</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 Champions Leagues (2005/06, 2008/09)</li>
                    <li>3 Leagues (1998/99, 2004/05, 2008/09)</li>
                    <li>Copa del Rey (2008/09)</li>
                    <li>2 Spanish Super Cups (2005/06, 2006/07)</li>
                    <li>2 Copas Catalunya (2003/04, 2004/05)</li>
                </ul>
                '
            ],
            [
                'name' => 'Ronaldo de Assís Moreira ‘Ronaldinho’',
                'country' => 'Cameroon',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/9fd0f501-9be1-4a14-9f7c-f5f4e6f8bdf9/4664952.jpg',
                'img_alt' => 'Ronaldinho',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'forward',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>The Brazilian striker (Porto Alegre,1980) joined the Club in 2003 and changed its history forever. In his five seasons at the club he became one its greatest ever footballers</p>

                <p>Ronaldinho came to Barcelona in the summer of 2003 to head the new club project which began with Frank Rijkaard as manager. The man who was to wear the number 10 shirt for Barça for five seasons immediately thrilled fans with his imaginative play which led to moves that very few footballers could aspire to. On 3 September 2003, on his league debut at Camp Nou, he scored a tremendous goal against Sevilla, cutting in from the wing and cracking in an unstoppable shot off the underside of the crossbar. </p>

                <p>During his time at Barça he scored 94 goals in competitive matches even though he also stood out as a great creator of goals. With him leading the team on the pitch, Barça experienced some of the best times in its history, winning two back-to-back League titles and above all the Champions League in an unforgettable final on 17 May 2006 in Paris.</p>

                <p>Also memorable was the standing ovation he received at Santiago Bernabéu after scoring two magnificent goals in the 3-0 win against Real Madrid on 19 November 2005. While at Barça he also won top international awards such as the FIFA World Player (2004 and 2005) and the Ballon d\'Or (2005).</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 2003-2008</li>
                    <li>Games: 250</li>
                    <li>Goals: 110</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>Champions Leagues (2005/06)</li>
                    <li>2 Leagues (2004/05, 2005/06)</li>
                    <li>2 Spanish Super Cups (2005/06, 2006/07)</li>
                    <li>3 Copas Catalunya (2003/04, 2004/05, 2006/07)</li>
                </ul>
                '
            ],



            [
                'name' => 'Anderson Luís de Souza(Deco)',
                'country' => 'Brazil',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/cf92b8e0-e57f-4b93-853b-f3932f5ba165/4584451.jpg',
                'img_alt' => 'Deco',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'midfielder',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Deco (Sao Bernardo, Brazil, 27-08-1977) was a player with a winning spirit and tactical intelligence</p>

                <p>Born in Brazil but a naturalised Portuguese international, Deco was one of the most important signings made by Barça for the 2004-05 season, Rijkaard’s second as manager. The midfielder came to the club after having won just about everything with Porto including the Champions League and being the best player of the competition in 2004. He was brought in to reinforce the core of the team following the departure of players such as Edgar Davids and Philip Cocu.</p>

                <p>His will to win, his tactical nous and his team spirit very soon made him into one of the leaders of one of the best teams in Barcelona’s history which won two back-to-back leagues and the Champions League. Deco showed that he was a complete footballer who combined commitment and hard work with the great technical skill often found among Brazilians. He was able to make the final pass, to create and to score but also to commit tactical fouls when necessary.</p>

                <p>At the end of the 2007-08 season and after four seasons and seven titles with Barça, Deco was transferred to Chelsea.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 2004-2008</li>
                    <li>Games: 188</li>
                    <li>Goals: 28</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 Ligas (2004-2005 and 2005-2006)</li>
                    <li>Champions League (2005-2006)</li>
                    <li>2 Spanish Super Cups (2005-2006 and 2006-2007)</li>
                    <li>75 international caps for Portugal</li>
                </ul>
                '
            ],
            [
                'name' => 'Patrick Kluivert',
                'country' => 'Netherlands',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/11/24/984910d2-25d8-45e0-bbf7-8627f88be248/mini_Kluivert.jpg?width=840&height=525',
                'img_alt' => 'Patrick Kluivert',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'forward',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>The Dutch striker (Amsterdam, Holland, 1976) came through the youth ranks at Ajax, going on to great success with the first team before joining AC Milan for a season and then Barça in the summer of 1998. As a blaugrana he scored 145 goals in 308 matches, making him one of the top scorers in the Club\'s history</p>

                <p>As a blaugrana he scored 145 goals in 308 matches, making him one of the top scorers in the Club\'s history. He was described by coach Louis van Gaal as the “perfect striker”, a good summation of an extraordinary forward who stood out for his agility and vision. He was excellent with his back to goal also, a talent that allowed to create as well as score goals. In fact, he was more of a creative midfielder who played as a striker than anything else.</p>

                <p>Kluivert loved to drop deep and link up with his team-mates as well as being an excellent finisher. Technically and physically he was a complete player who spent six seasons as a blaugrana before leaving for Newscastle United in 2004.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1998-2004</li>
                    <li>Games: 308</li>
                    <li>Goals: 145</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>La Liga (1999)</li>
                    <li>Champions League winner with Ajax (1995)</li>
                    <li>79 caps for the Netherlands</li>
                </ul>
                '
            ],
            [
                'name' => 'Rivaldo Vítor Borba Ferreira',
                'country' => 'Brazil',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/ab30dcfe-3d5a-4d23-a029-7b3f0ef4637c/14908659.jpg',
                'img_alt' => 'Rivaldo',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'forward',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>A Brazilian striker of undoubted quality who was a magician with the ball, an exceptional dribbler and a great goalscorer who came to Barça from Deportivo La Coruña in 1997 and stayed until his departure for AC Milan in 2002. Rivaldo’s hung up his boots back in his native Brazil in 2013 with Sao Caetano.</p>

                <p>Rivaldo came to FC Barcelona in August 1997 with job of replacing his fellow Brazilian Ronaldo who had just left for Inter Milan. On his league debut for the blaugranes the striker scored twice in a win over Real Sociedad. His first season at Barça ended with 28 goals in all competitions and winners’ medals in the League, Copa del Rey and the European Super Cup.</p>

                <p>The following season, FC Barcelona’s centenary, was also a successful one with Rivaldo picking up another league title and the Ballon d’Or and FIFA World Player individual awards.</p>

                <p>The Brazilian’s last few seasons as a blaugrana did not bring so much success in terms of trophies but they did provide some memorable moments such as his wonderful overhead kick, one of a hat-trick, against Valencia in the 2000/2001 season which handed Barça a place in the Champions League for the following season.</p>

                <p>After five seasons with FC Barcelona the Brazilian left for AC Milan in 2002 and won the Champions League at Old Trafford, beating Juventus in the final. Spells in Greek, Angolan and finally back in his native Brazil with Sao Caetano, Rivaldo hung up his boots in 2013.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1997-2002</li>
                    <li>Games: 253</li>
                    <li>Goals: 136</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>2 Spanish Leagues (1997-1998 and 1998-1999)</li>
                    <li>Copa del Rey (1997-1998)</li>
                    <li>UEFA Super Cup (1997-1998)</li>
                    <li>Ballon d’Or and FIFA World Player (1999)</li>
                    <li>World Cup winner with Brazil (2002)</li>
                    <li>Champions League winner with AC Milan (2003)</li>
                    <li>75 international caps for Brazil</li>
                </ul>
                '
            ],
            [
                'name' => 'Luis Enrique Martínez',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/dcebc5df-51ab-4e73-9ce9-d7bae13d71c2/3931180.jpg',
                'img_alt' => 'Luis Enrique',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'midfielder',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>The popular ‘Lucho\' (Gijón, 1970) came to FC Barcelona in the summer of 1996 as a free agent after having played at Real Madrid for five seasons</p>

                <p>Despite his Madrid past, he adapted quickly to his new club, and soon became one of the most charismatic players ever to represent FC Barcelona. striker scored twice in a win over Real Sociedad. His first season at Barça ended with 28 goals in all competitions and winners’ medals in the League, Copa del Rey and the European Super Cup.</p>

                <p>He was a born leader, and highly committed, for which reason Luis Enrique was one of the team captains. His finest quality was his versatility, the characteristic that best describes his way of playing, although there were so many more. He normally played as a linkman with the out and out striker, although he would also play in wider positions, or even as centre forward.</p>

                <p>He was a skilled and tireless worker, and a great goalscorer too, especially when coming from behind, where he was never was one to throw in the towel, even in the most difficult of situations. He would constantly encourage his colleagues, and was an important squad member both on and off the pitch. The character and flair of this Asturian set an example for the fans to admire. He stayed at Barça until he finally hung up his boots in 2004.</p>

                <p>Between 2008 and 2011 Luis Enrique was coach at Barça B. Three years later, after spells in charge of AS Roma and Celta Vigo, he returned to the Club as first team coach, winning an historic treble in his very first season. In 2017 he left the Club with an extraordinary record of having won nine trophies out of a possible 13</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1996-2004</li>
                    <li>Games: 355</li>
                    <li>Goals: 123</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>Cup Winners Cup (1996/97)</li>
                    <li>European Super Cup (1997)</li>
                    <li>2 Copas del Rey (1996/97 and 97/98)</li>
                    <li>2 Leagues (1997/98 and 98/99)</li>
                    <li>Spanish Super Cup (1996)</li>
                    <li>2 Copas Catalunya (1999/00, 2003/04)</li>
                </ul>
                '
            ],
            [
                'name' => 'Josep Guardiola',
                'country' => 'Spain',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/b2444f08-cf47-4db2-bef5-bd51afbffee6/4585894.jpg',
                'img_alt' => 'Josep Guardiola',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'midfielder',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>A gifted midfielder with an exquisite technique and an ability to read the game, he was the fulcrum of the team with the famous number \'4\' on his back</p>

                <p>Known as the \'boy from Santpedor\', after coming up through the youth teams at FC Barcelona, he made his first team debut in the 1990/91 season. His brilliant playing career coincided with one of the most glorious times in the Club\'s history, the age of the \'Dream Team\' presided over by coach Johan Cruyff who said that Guardiola was an extension of himself out on the field. Pep playing in the same XI in the 1990s as some of the best players in the world: Romário, Laudrup, Bakero, Zubizarreta, Stòitxkov, Koeman, Sergi, Ferrer, Amor, Figo, Ronaldo, Rivaldo and Luis Enrique, amongst others. One bump in the road during his wonderful playing career was the serious injury he suffered in the 1997/98 season that kept him out for a year. </p>

                <p>He finally left Barça in 2001 to sign for Italian club Brescia. On the international stage his highlight was winning a goal medal with the Spanish national side in the 1992 Olympic Games in Barcelona.</p>

                <p>He became Barça B coach in 2007 and then first team coach in 2008, going on to be the most successful coach in the Club\'s history with 14 trophies in four seasons. winning six in 2009 alone. </p>

                <p>In 2010 was give the Gold Medal for Sporting Merit and in 2011 the Medal of Honour from the Catalan Parliament. </p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1990-2001</li>
                    <li>Games: 479</li>
                    <li>Goals: 16</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>Cup Winners Cup (1996/97)</li>
                    <li>European Cup (1991/92)</li>
                    <li>2 European Super Cups (1992 and 1997)</li>
                    <li>2 Copas del Rey (1996/97 and 97/98)</li>
                    <li>6 Leagues (1990/91, 91/92, 92/93, 93/94, 97/98 and 98/99)</li>
                    <li>4 Spanish Super Cups (1991, 1992, 1994 and 1996)</li>
                    <li>Copas Catalunya (1999/00)</li>
                    <li>2 Copes de Generalitat (1990/91, 92/93)</li>
                </ul>
                '
            ],
            [
                'name' => 'Johan Cruyff',
                'country' => 'nederland',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/efba8ccb-2097-48ed-88ae-3b4b8a78489f/20489661.jpg',
                'img_alt' => 'Johan Cruyff',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'forward',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Cruyff (Amsterdam, 25/04/1947 - Barcelona, 24/03/2016) was one of the best players in the world of all time and in his time, he was considered number one</p>

                <p>His immense quality saw him win worldwide recognition and he won the Golden ball award three times (1971, 1973 and 1974). </p>

                <p>The Dutchman signed for FC Barcelona in August 1973 following lengthy negotiations with his former club Ajax. In his first season, Cruyff exploded onto the scene; using his technical brilliance and intelligence to help Barça win the league in 1973-74 – the first time in 14 years.</p>

                <p>In the eyes of the fans, they will always remember many images of the goal he scored against Athletico Madrid in Camp Nou and the famous 0-5 victory over Madrid in the Bernabeu in 1974, which will go down in history. In the following seasons Barça were able only to win the Copa del Rey in 1977-78. Cruyff left the club in 1978. </p>

                <p>In 1988, he returned to the club as coach and put into motion the best era in the history of the club: winning 4 league titles among other and winning the European cup for the first time at Wembley in 1992 with the ‘Dream Team’. Cruyff then left the club in 1996. </p>

                <p>He received two tributes at Camp Nou, the first when he left as a player on 27 May, 1978 and the second with the \'Dream Team\' that he coached on 10 March 1999. </p>

                <p>In 1999 he was chosen as best player of the century in Europe and in September 2006 he was awarded the cross of Saint Jordi from the Government of Catalonia.</p>

                <p>Cruyff died on 24 March 2016 after losing his battle with lung cancer.</p>

                <p>Posthumously, the Spanish Government awarded him the Gold Medal of Royal Sporting Merit. Also posthumously, on 18 April he was given the Laureus Award for the spirit of sport. On 1 July 2016, the Barcelona City Council awarded him the Gold Medla for Sporting Merit whoich was collected by his family on 10 November at the Saló de Cent at the City Council in Barcelona. </p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1973-78</li>
                    <li>Games: 231</li>
                    <li>Goals: 86</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>League (1973/74)</li>
                    <li>King’s Cup (1977/78)</li>
                </ul>
                '
            ],
            [
                'name' => 'Ronald Koeman',
                'country' => 'Dutch',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/03/19/f31308da-efb9-41b4-bacb-f42c8a052d6c/4585829.jpg',
                'img_alt' => 'Ronald Koeman',
                'date_of_birth' => null,
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'defender',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>\'Tintin\' Koeman will always get a mention in FC Barcelona history for scoring the goal that handed Barça victory in the 1992 European Cup at Wembley</p>

                <p>The Dutch defender (Zaandam, 1963) was one of the central figures in the Dream Team, being a masterful player just in front of the defence and being the provider of amazing pinpoint passes that generated so many dangerous opportunities. </p>

                <p>He will also always be remembered for being one of the most reliable penalty takers the sport has ever known, for his free-kick abilities, and for the record breaking power he could put behind the ball. He scored 106 goals in his six years at Barça, an incredible tally for a defensive player. in fact, he is the defender with most goals in the Club\'s history. Off the pitch, he was popular for his pleasant, down to earth nature, and he became one of the undisputed club idols of the era. He was part of the club’s coaching staff from July 1998 to December 1999.</p>

                <h3>Career</h3>
                <ul>
                    <li>Seasons at the Club: 1989-95</li>
                    <li>Games: 350</li>
                    <li>Goals: 106</li>
                </ul><br/>

                <h3>Honours</h3>
                <ul>
                    <li>European Cup (1991/92)</li>
                    <li>European Super Cup (1992)</li>
                    <li>4 Leagues (1990/91, 91/92, 92/93, 93/94)</li>
                    <li>Copa del Rey (1989/90)</li>
                    <li>3 Spanish Super Cups (1991, 1992 and 1995)</li>
                    <li>2 Copa Generalitat (1990/91 and 92/93)</li>
                </ul>
                '
            ],
            [
                'name' => 'Eric Abidal',
                'country' => 'France',
                'img_src' => 'https://www.fcbarcelona.com/photo-resources/2019/11/24/cb5e3420-cb0c-442b-aa0f-963ae7e699da/mini_Abdial.jpg?width=840&height=525',
                'img_alt' => 'Eric Abidal',
                'date_of_birth' => '11/09/1979',
                'number' => null,
                'club_debut' => null,
                'weight' => null,
                'height' => null,
                'position' => 'defender',
                'work_foot' => 'right',
                'legend' => 1,
                'content' => '
                <p>Excellent left back who arrived at the club on the back of the prestige he’d gained at Olympique Lyonnais and for the French national team. Arrived in the last season with Frank Rijkaard on the Barça bench (2007/2008) and quickly commandeered the left flank of the defence. In the following season he was a key part of the team that won Barça’s first ever treble with Guardiola as coach, which would become a sextet at the end of an unforgettable 2009. His physical strength and speed made him an essential player, and he could also shoot. As a Barça player he beat liver cancer, which was why captain Carles Puyol handed him the honour of lifting the Champions League trophy at Wembley in 2011. He has gone on to do a lot for charity causes. After six seasons at Barça he signed for Monaco in France. Finally he’d end up retiring at Olimpiacos in 2015.</p>

                <p>As well as the many titles and his importance as a Barça player, Eric Abidal will be especially remembered for his first goal as a blaugrana. It took a long time coming but eventually came during the new year celebrations of 2011, at the cathedral of San Mamés in the first leg of the round of 16 in the Copa del Rey. His goal meant Barça returned from Bilbao with a valuable draw. Despite his liver tumour and transplant, Abidal remained part of that triumphant Barça team and posted some of his finest performances for the side. At the end of his contract in 2013 he left for the French and then the Greek championships. But he would eventually return, becoming technical secretary of the club.</p>

                <h3>Honours</h3>
                <ul>
                    <li>2 World Club Cups (2009/10, 2011/12)</li>
                    <li>2 Champions Leagues (2008/09, 2010/11)</li>
                    <li>2 European Super Cups  (2009/10, 2011/12)</li>
                    <li>4 Leagues (2008/09, 2009/10, 2010/11, 2012/13)</li>
                    <li>2 Copas del Rey (2008/09, 2011/12)</li>
                    <li>3 Spanish Super Cups (2009/10, 2010/11, 2011/12)</li>
                </ul>
                '
            ],
        ]);
    }
}
