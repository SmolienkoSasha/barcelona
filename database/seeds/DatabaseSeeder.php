<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(GalleriesTableSeeder::class);
         $this->call(NewsTableSeeder::class);
         $this->call(TeamPlayersTableSeeder::class);
         $this->call(TeamCoachStaffTableSeeder::class);
         $this->call(TeamManagementTableSeeder::class);
         $this->call(PagesTableSeeder::class);
         $this->call(HistoryTitlesTableSeeder::class);
    }
}
