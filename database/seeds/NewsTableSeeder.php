<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            [
                'title' => 'Barcelona vs Borussia Dortmund, player ratings: Lionel Messi runs Champions League show',
                'content' => '<p>
                                Barcelona and Lionel Messi proved too much for Borussia Dortmund as the hosts cruised to victory at the Nou Camp.
                            </p>
                            <p>
                                Messi scored one goal and assisted the other two for Luis Suarez and substitute Antoine Griezmann as Barca racked up three, before Jadon Sancho\'s brilliant late reply made it 3-1.
                            </p>
                            <p>
                                The victory means Barcelona secured their place in the last 16 of the Champions League, while Dortmund have it all to play for.
                            </p>
                            <p>
                                Dortmund will play Slavia Prague in their final group game while Inter Milan, who are now level with the German team on seven points, face Barcelona.
                            </p>
                            <p>
                                Should Inter and Dortmund finish on the same points, Inter will progress courtesy of their better head-to-head record.
                            </p>',
                'img_src' => 'https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/11/27/21/Lionel-Messi-.jpg?w968',
                'img_alt' => 'Lionel Messi celebrates scoring Barcelona\'s second goal ',
                'created_at' => '2019-12-1 21:51:45'
            ],
            [
                'title' => 'Inter CEO Marotta drops massive hint at Vidal\'s possible move',
                'content' => '<p>
                                Inter Milan\'s CEO Giuseppe Marotta has once again stressed Arturo Vidal\'s potential transfer move to the Nerazzurri.
                            </p>
                            <p>
                                "We have a short squad. Vidal is a world-class player," said Inter official.
                            </p>
                            <p>
                               Shortly before that, the club\'s head coach Antonio Conte hailed Arturo and recalled his time at Juventus.
                            </p>
                            <p>
                                By the way, Giuseppe Marotta was the CEO of Juventus at the time when Vidal played for the Old Lady. The 62-year-old could have contacted the Chilean\'s representatives \'for old times\' sake\'.
                            </p>
                            <p>
                               Arturo Vidal has been heavily linked with Inter Milan move over the latest months. The Chilean has recently told that he will go for an exit if he feels he\'s \'no longer important\' at Barca.
                            </p>
                             <p>
                               Vidal has been one of the most impressive performers for Barcelona since the season\'s start. The 32-year-old has scored 4 goals and provided an assist in 13 appearances this season.
                            </p>',
                'img_src' => 'https://90l.tribuna.com/images/d1/63/40/d16340710fe8416b857676f7a9c40103500x500@2x.jpg',
                'img_alt' => 'Vidal rumor',
                'created_at' => '2019-12-1 20:51:45'
            ]
        ]);
    }
}
