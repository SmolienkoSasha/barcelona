<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galleries')->insert([
            [
                'src' => 'https://d.newsweek.com/en/full/1520689/fc-barcelona-antoine-griezmann.webp?w=737&f=71fcd6ab794ad96bcfbfeb090865b1b7',
                'alt' => 'Grizman',
                'description' => 'Grizman scored',
                'created_at' => '2019-12-1 18:51:45'
            ],
            [
                'src' => 'https://90l.tribuna.com/images/d1/63/40/d16340710fe8416b857676f7a9c40103500x500@2x.jpg',
                'alt' => 'Barcelona',
                'description' => 'Grizman scored',
                'created_at' => '2019-12-1 20:46:55'
            ]

        ]);
    }
}
