<?php

use Illuminate\Database\Seeder;

class TeamManagementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team_managements')->insert([
            [
                'category' => 'PROFESSIONAL FOOTBALL',
                'content' => '
                <p>TECHNICAL SECRETARY PROFESSIONAL FOOTBALL: Éric Abidal</p>
                <p>ADJUNCT TECHNICAL SECRETARY PROFESSIONAL FOOTBALL: Ramon Planes</p>
                <p>FIRST TEAM COACH: Ernesto Valverde</p>
                <p>TECHNICAL SECRETARY BARÇA B: José Mari Bakero</p>
                <p>BARÇA B COACH: Francesc Xavier Garcia Pimienta</p>
                '
            ],
            [
                'category' => 'YOUTH FOOTBALL',
                'content' => '
                <p>HEAD OF YOUTH FOOTBALL: Patrick Kluivert</p>
                <p>TECHNICALYOUTH FOOTBALL: Jordi Roura</p>
                <p>HEAD OF FUTBOL 11: Aureli Altimira</p>
                <p>HEAD OF FUTBOL 7: Quim Estrada</p>
                <p>COACH U19A: Franc Artiga</p>
                <p>COACU U19B: -</p>
                <p>COACH U16A: Sergi Milà</p>
                <p>COACH U16B: Marc Serra</p>
                <p>COACH U14A: David Sánchez</p>
                <p>COACH U14B: Albert Puig</p>
                <p>COACH U12A: Jordi Pérez</p>
                <p>COACH U12B: Èric Campos</p>
                <p>COACH U12C: Álex Urrestarazu</p>
                <p>COACH U12D: Pau Moral</p>
                <p>COACH U10A: Juan Antonio Gil</p>
                <p>COACH U10B: Mario García</p>
                <p>COACH U10C: David Sánchez Aradilla</p>
                <p>COACH U10D: Mario Jordano</p>
                <p>COACH U8: Daniel Segovia</p>
                '
            ],
            [
                'category' => 'OTHER',
                'content' => '
                <p>INSTITUTIONAL AND SPORTING RELATIONS FOOTBALL: Guillermo Amor</p>
                <p>GOALKEEPER COORDINATOR: Ricard Segarra</p>
                <p>HEAD OF SCOUTING: Josep Boada</p>
                '
            ],
        ]);
    }
}
