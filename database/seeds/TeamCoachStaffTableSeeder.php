<?php

use Illuminate\Database\Seeder;

class TeamCoachStaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team_coach_staff')->insert([
            [
                'name' => 'Ernesto Valverde',
                'position' => 'Manager',
                'img_src' => 'https://fcbarcelona-static-files.s3.amazonaws.com/fcbarcelona/photo/2018/10/26/242a9124-7058-4fd8-a121-f59a4cd3fc80/VALVERDE-JOC.jpg',
                'img_alt' => 'Ernesto Valverde',
                'content' => '
                <p>Ernesto Valverde (9 February 1964, Viandar de la Vera, Cáceres) was presented as the new FC Barcelona coach on 1 June 2017 after leaving Athletic Club Bilbao where he coached the team in more matches than anyone else in its history.</p>

                <p>After three seasons with Luis Enrique Martínez as coach, FC Barcelona decided to acquire the services of the a coach who is well known for his man management skills, his attacking style and his extensive experience.</p>

                <p>Valverde began his coaching career in the youth system at Athletic Club, beginning in 1997/98 with the U16B side. He moved up through the coaching ranks at the Basque club and between 2003 and 2005 he enjoyed his first spell in charge of the first team.</p>

                <p>After a sabbatical year, Valverde returned to coaching at Barcelona’s other club, Espanyol. He took the team to the final of the UEFA Cup on 2007 where they were beaten by fellow Spanish club Sevilla before departing in the summer of 2008.</p>

                <p>Valverde went to coach other La Liga sides, Villarreal (2009/10) and Valencia (2012/13) as well as two separate spells at Greek club Olympiakos between 2008-09 and then 2010-2012 where he won the Greek league on no less than three occasions and the Greek Cup twice.</p>

                <p>In 2013 he returned to Athletic Club for his final coaching job before joining the blaugranes. In each of his four seasons in charge he took the club into Europe and in 2016 he led the team to their first trophy in 31 years when they beat Barça in the Spanish Super Cup.</p>
                '
            ],
            [
                'name' => 'JON ASPIAZU',
                'position' => 'ASSISTANT COACH',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'JOAN BARBARÀ',
                'position' => 'COACHING ASSISTANT',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'JOSÉ ANTONIO POZANCO, EDU PONS AND ANTONIO GÓMEZ',
                'position' => 'FITNESS COACHES',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'JOSÉ RAMÓN DE LA FUENTE',
                'position' => 'GOALKEEPING COACH',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'CARLES NAVAL',
                'position' => 'DELEGATE',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'RICARD PRUNA, XAVIER YANGUAS AND DANIEL FLORIT',
                'position' => 'CLUB DOCTORS',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
            [
                'name' => 'JUANJO BRAU, XAVI LINDE, XAVI LÓPEZ, XAVIER ELAIN, JORDI MESALLES, SEBAS SALAS AND DANIEL BENITO',
                'position' => 'PHYSIOS',
                'img_src' => null,
                'img_alt' => null,
                'content' => null
            ],
        ]);
    }
}
