<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 155);
            $table->string('date_of_birth', 100)->nullable();
            $table->string('weight', 50)->nullable();
            $table->string('height', 50)->nullable();
            $table->string('club_debut', 100)->nullable();
            $table->string('country', 100);
            $table->text('img_src');
            $table->string('img_alt', 100);
            $table->text('content');
            $table->string('position', 100);
            $table->string('work_foot', 100);
            $table->boolean('legend');
            $table->unsignedSmallInteger('number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_players');
    }
}
